#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>

#include "mu-mips.h"
#include "mu-cache.h"

/***************************************************************/
/* Print out a list of commands available                                                                  */
/***************************************************************/
void help() {
	printf("------------------------------------------------------------------\n\n");
	printf("\t**********MU-MIPS Help MENU**********\n\n");
	printf("sim\t\t\t-- simulate program to completion \n");
	printf("run <n>\t\t\t-- simulate program for <n> instructions\n");
	printf("rdump\t\t\t-- dump register values\n");
	printf("cdump\t\t\t-- dump cache block values\n");
	printf("reset\t\t\t-- clears all registers/memory and re-loads the program\n");
	printf("input <reg> <val>\t-- set GPR <reg> to <val>\n");
	printf("mdump <start> <stop>\t-- dump memory from <start> to <stop> address\n");
	printf("high <val>\t\t-- set the HI register to <val>\n");
	printf("low <val>\t\t-- set the LO register to <val>\n");
	printf("print\t\t\t-- print the program loaded into memory\n");
	printf("show\t\t\t-- print the current content of the pipeline registers\n");
	printf("Forwarding <1/0>\t-- To Enable or disable forwarding\n");
	printf("Associativity<2,4,8,0>  -- Setting Cache associativity(0= full)\n");
	printf("?\t\t\t-- display help menu\n");
	printf("quit\t\t\t-- exit the simulator\n\n");
	printf("------------------------------------------------------------------\n\n");
}

/***************************************************************/
/* Read a 32-bit word from memory                                                                            */
/***************************************************************/
uint32_t mem_read_32(uint32_t address)
{
	int i;
	for (i = 0; i < NUM_MEM_REGION; i++) {
		if ( (address >= MEM_REGIONS[i].begin) &&  ( address <= MEM_REGIONS[i].end) ) {
			uint32_t offset = address - MEM_REGIONS[i].begin;
			return (MEM_REGIONS[i].mem[offset+3] << 24) |
					(MEM_REGIONS[i].mem[offset+2] << 16) |
					(MEM_REGIONS[i].mem[offset+1] <<  8) |
					(MEM_REGIONS[i].mem[offset+0] <<  0);
		}
	}
	return 0;
}

/***************************************************************/
/* Write a 32-bit word to memory                                                                              */
/***************************************************************/
void mem_write_32(uint32_t address, uint32_t value)
{
	int i;
	uint32_t offset;
	for (i = 0; i < NUM_MEM_REGION; i++) {
		if ( (address >= MEM_REGIONS[i].begin) && (address <= MEM_REGIONS[i].end) ) {
			offset = address - MEM_REGIONS[i].begin;

			MEM_REGIONS[i].mem[offset+3] = (value >> 24) & 0xFF;
			MEM_REGIONS[i].mem[offset+2] = (value >> 16) & 0xFF;
			MEM_REGIONS[i].mem[offset+1] = (value >>  8) & 0xFF;
			MEM_REGIONS[i].mem[offset+0] = (value >>  0) & 0xFF;
		}
	}
}

/***************************************************************/
/* Execute one cycle                                                                                           */
/***************************************************************/
void cycle() {
	handle_pipeline();
	CURRENT_STATE = NEXT_STATE;
	CYCLE_COUNT++;
}

/***************************************************************/
/* Simulate MIPS for n cycles                                                                                  */
/***************************************************************/
void run(int num_cycles) {

	if (RUN_FLAG == FALSE) {
		printf("Simulation Stopped\n\n");
		return;
	}

	printf("Running simulator for %d cycles...\n\n", num_cycles);
	int i;
	for (i = 0; i < num_cycles; i++) {
		if (RUN_FLAG == FALSE) {
			printf("Simulation Stopped.\n\n");
			break;
		}
		cycle();
	}
}

/***************************************************************/
/* simulate to completion                                                                                      */
/***************************************************************/
void runAll() {
	if (RUN_FLAG == FALSE) {
		printf("Simulation Stopped.\n\n");
		return;
	}

	printf("Simulation Started...\n\n");
	while (RUN_FLAG){
		cycle();
	}
	printf("Simulation Finished.\n\n");
}

/***************************************************************/
/* Dump a word-aligned region of memory to the terminal                              				*/
/***************************************************************/
void mdump(uint32_t start, uint32_t stop) {
	uint32_t address;

	printf("-------------------------------------------------------------\n");
	printf("Memory content [0x%08x..0x%08x] :\n", start, stop);
	printf("-------------------------------------------------------------\n");
	printf("\t[Address in Hex (Dec) ]\t[Value]\n");
	for (address = start; address <= stop; address += 4){
		printf("\t0x%08x (%d) :\t0x%08x\n", address, address, mem_read_32(address));
	}
	printf("\n");
}

/***************************************************************/
/* Dump current values of registers to the teminal                                              		*/
/***************************************************************/
void rdump() {
	int i;
	printf("-------------------------------------\n");
	printf("Dumping Register Contenter\n");
	printf("-------------------------------------\n");
	printf("# Instructions Executed\t: %u\n", INSTRUCTION_COUNT);
	printf("# Cycles Executed\t: %u\n", CYCLE_COUNT);
	printf("PC\t: 0x%08x\n", CURRENT_STATE.PC);
	printf("-------------------------------------\n");
	printf("[Register]\t[Value]\n");
	printf("-------------------------------------\n");
	for (i = 0; i < MIPS_REGS; i++){
		printf("[R%d]\t: 0x%08x\n", i, CURRENT_STATE.REGS[i]);
	}
	printf("-------------------------------------\n");
	printf("[HI]\t: 0x%08x\n", CURRENT_STATE.HI);
	printf("[LO]\t: 0x%08x\n", CURRENT_STATE.LO);
	printf("-------------------------------------\n");
}
/***************************************************************/
/* Dump current values of Cache blocks to the teminal                                              		*/
/***************************************************************/
void cdump() {
	int i;
	printf("-------------------------------------\n");
	printf("Dumping Cache Content\n");
	printf("-------------------------------------\n");
	printf("# Instructions Executed\t: %u\n", INSTRUCTION_COUNT);
	printf("# Cycles Executed\t: %u\n", CYCLE_COUNT);
	printf("# Cache hits\t: %u\n", cache_hits);
	printf("# Cache misses\t: %u\n", cache_misses);
	printf("PC\t: 0x%08x\n", CURRENT_STATE.PC);
	printf("-------------------------------------\n");
	printf("|Index|\tValid|\tTag     |Data 1  |Data 2  |Data 3  |Data 4  |\n");
	for (i = 0; i < NUM_CACHE_BLOCKS; i++){		
		printf("|   %2d|\t   %d|\t%08x|%08x|%08x|%08x|%08x|\n",(i+1),L1Cache.blocks[i].valid,
										    L1Cache.blocks[i].tag,
									            L1Cache.blocks[i].words[0],
									   	    L1Cache.blocks[i].words[1],
									  	    L1Cache.blocks[i].words[2],
									  	    L1Cache.blocks[i].words[3]);
	}	
}
	
/***************************************************************/
/* Read a command from standard input.                                                               		*/
/***************************************************************/
void handle_command() {
	char buffer[20];
	uint32_t start, stop, cycles;
	uint32_t register_no;
	int register_value;
	int hi_reg_value, lo_reg_value;

	printf("MU-MIPS SIM:> ");

	if (scanf("%s", buffer) == EOF){
		exit(0);
	}

	switch(buffer[0]) {
		case 'S':
		case 's':
			if (buffer[1] == 'h' || buffer[1] == 'H'){
				show_pipeline();
			}else {
				runAll();
			}
			break;
		case 'M':
		case 'm':
			if (scanf("%x %x", &start, &stop) != 2){
				break;
			}
			mdump(start, stop);
			break;
		case 'C':
		case 'c':
			cdump();
			break;
		case '?':
			help();
			break;
		case 'Q':
		case 'q':
			printf("**************************\n");
			printf("Exiting MU-MIPS! Good Bye...\n");
			printf("**************************\n");
			exit(0);
		case 'R':
		case 'r':
			if (buffer[1] == 'd' || buffer[1] == 'D'){
				rdump();
			}else if(buffer[1] == 'e' || buffer[1] == 'E'){
				reset();
			}
			else {
				if (scanf("%d", &cycles) != 1) {
					break;
				}
				run(cycles);
			}
			break;
		case 'I':
		case 'i':
			if (scanf("%u %i", &register_no, &register_value) != 2){
				break;
			}
			CURRENT_STATE.REGS[register_no] = register_value;
			NEXT_STATE.REGS[register_no] = register_value;
			break;
		case 'H':
		case 'h':
			if (scanf("%i", &hi_reg_value) != 1){
				break;
			}
			CURRENT_STATE.HI = hi_reg_value;
			NEXT_STATE.HI = hi_reg_value;
			break;
		case 'L':
		case 'l':
			if (scanf("%i", &lo_reg_value) != 1){
				break;
			}
			CURRENT_STATE.LO = lo_reg_value;
			NEXT_STATE.LO = lo_reg_value;
			break;
		case 'P':
		case 'p':
			print_program();
			break;
		case 'F':
		case 'f':
			if(scanf("%d", &ENABLE_FORWARDING) != 1){
				break;			
			}
			ENABLE_FORWARDING == 0 ? printf("Forwarding OFF\n") : printf("Forwarding ON\n");
			break;
		default:
			printf("Invalid Command.\n");
			break;
	}
}

/***************************************************************/
/* reset registers/memory and reload program                                                    		*/
/***************************************************************/
void reset() {
	int i;
	/*reset registers*/
	for (i = 0; i < MIPS_REGS; i++){
		CURRENT_STATE.REGS[i] = 0;
	}
	CURRENT_STATE.HI = 0;
	CURRENT_STATE.LO = 0;	
	for (i = 0; i < NUM_MEM_REGION; i++) {
		uint32_t region_size = MEM_REGIONS[i].end - MEM_REGIONS[i].begin + 1;
		memset(MEM_REGIONS[i].mem, 0, region_size);
	}

	/*load program*/
	load_program();
	
	/*Empty Cache*/
	init_cache();

	/*reset PC*/
	INSTRUCTION_COUNT = 0;
	CYCLE_COUNT = 0;
	ENABLE_FORWARDING = 0;
	CURRENT_STATE.PC =  MEM_TEXT_BEGIN;
	NEXT_STATE = CURRENT_STATE;
	RUN_FLAG = TRUE;
	TAKEN_FLAG = FALSE;
	BRANCH_FLAG = FALSE;
}

/***************************************************************/
/* Allocate and set memory to zero                                                                            	*/
/***************************************************************/
void init_memory() {
	int i;
	for (i = 0; i < NUM_MEM_REGION; i++) {
		uint32_t region_size = MEM_REGIONS[i].end - MEM_REGIONS[i].begin + 1;
		MEM_REGIONS[i].mem = malloc(region_size);
		memset(MEM_REGIONS[i].mem, 0, region_size);
	}
}
/***************************************************************/
/* Initializing Cahce.                                                                                                        */
/***************************************************************/
void init_cache() {
	int i;
	for (i = 0; i < NUM_CACHE_BLOCKS; i++){
		L1Cache.blocks[i].valid = 0;
		L1Cache.blocks[i].tag = 0x00000000;
		L1Cache.blocks[i].words[0] = 0x00000000;
		L1Cache.blocks[i].words[1] = 0x00000000;
		L1Cache.blocks[i].words[2] = 0x00000000;
		L1Cache.blocks[i].words[3] = 0x00000000;
	}
}

/**************************************************************/
/* load program into memory                                                                                   	*/
/**************************************************************/
void load_program() {
	FILE * fp;
	int i, word;
	uint32_t address;

	/* Open program file. */
	fp = fopen(prog_file, "r");
	if (fp == NULL) {
		printf("Error: Can't open program file %s\n", prog_file);
		exit(-1);
	}

	/* Read in the program. */

	i = 0;
	while( fscanf(fp, "%x\n", &word) != EOF ) {
		address = MEM_TEXT_BEGIN + i;
		mem_write_32(address, word);
		printf("writing 0x%08x into address 0x%08x (%d)\n", word, address, address);
		i += 4;
	}
	PROGRAM_SIZE = i/4;
	printf("Program loaded into memory.\n%d words written into memory.\n\n", PROGRAM_SIZE);
	fclose(fp);
}
/************************************************************/
/* maintain cache                                                                                          */
/************************************************************/
uint32_t find_in_cache( uint32_t address){
	
	uint32_t tag;
	int index;
	int cache_miss;	
	int word_index;
	cache_miss = cache_misses;
	printf("\n\n%d\t%dqwertty\n",cache_misses,cache_miss);
	tag = (address & 0xFFFFFFF0);
	index = (address & 0x000000F0) >> 4;
	word_index =(address & 0x0000000C) >> 2;
	printf("%08x \t %08x \t %d\t%d",address,(address & 0x0000000C),word_index,index);
	if(MEM_WB.RegWrite)
	{
		if((tag == L1Cache.blocks[index].tag) && (L1Cache.blocks[index].valid != 0))
		{
			cache_hits++;
			return 	L1Cache.blocks[index].words[word_index];			
		}
		else
		{
			L1Cache.blocks[index].words[0]= mem_read_32(address & 0xFFFFFFF3); //00(first of the word block)
			L1Cache.blocks[index].words[1]= mem_read_32(address & 0xFFFFFFF7); //01
			L1Cache.blocks[index].words[2]= mem_read_32(address & 0xFFFFFFFB); //10
			L1Cache.blocks[index].words[3]= mem_read_32(address & 0xFFFFFFFF); //11
			L1Cache.blocks[index].tag = tag;
			cache_misses++;
		}
	}
	else
	{
		if((L1Cache.blocks[index].valid != 0) && (tag == L1Cache.blocks[index].tag))
		{
			L1Cache.blocks[index].words[word_index] = EX_MEM.B;
			cache_hits++;			
		}
		else
		{
			L1Cache.blocks[index].words[0]= mem_read_32(address & 0xFFFFFFF3); //00(first of the word block)
			L1Cache.blocks[index].words[1]= mem_read_32(address & 0xFFFFFFF7); //01
			L1Cache.blocks[index].words[2]= mem_read_32(address & 0xFFFFFFFB); //10
			L1Cache.blocks[index].words[3]= mem_read_32(address & 0xFFFFFFFF); //11
			L1Cache.blocks[index].words[word_index] = EX_MEM.B;
			L1Cache.blocks[index].tag = tag;
			cache_misses++;
		}
	}
	cache_miss = cache_misses - cache_miss;
//Updating main memory
	if((MEM_WB.RegWrite == 0) || cache_miss)
	{
		mem_write_32(((address & 0xFFFFFFF3) | 0x00000000),L1Cache.blocks[index].words[0]); //00(first of the word block)
		mem_write_32(((address & 0xFFFFFFF7) | 0x00000004),L1Cache.blocks[index].words[1]); //01
		mem_write_32(((address & 0xFFFFFFFB) | 0x00000008),L1Cache.blocks[index].words[2]); //10
		mem_write_32(((address & 0xFFFFFFFF) | 0x0000000C),L1Cache.blocks[index].words[3]); //11
	}
//Adding incrementing the cycle count if there's a miss
	if(cache_miss)
	{
		CYCLE_COUNT+=99;
	}
	L1Cache.blocks[index].valid = 1;
	return 	L1Cache.blocks[index].words[word_index];
}

/************************************************************/
/* maintain the pipeline                                                                                          */
/************************************************************/
void handle_pipeline()
{
	/*INSTRUCTION_COUNT should be incremented when instruction is done*/
	/*Since we do not have branch/jump instructions, INSTRUCTION_COUNT should be incremented in WB stage */

	WB();
	MEM();
	EX();
	ID();
	IF();		
}

/************************************************************/
/* writeback (WB) pipeline stage:                                                                          */
/************************************************************/
void WB()
{
	/*IMPLEMENT THIS*/

	// Put variables we need here
	// uint32_t instruction;
	uint32_t opcode, function;
	// uint32_t rs;
	uint32_t rt, rd;
	uint32_t sa;
	// uint32_t immediate;
	// uint32_t target;
	// uint64_t product, p1, p2;
	// uint32_t addr;
	// uint32_t data;
	// int branch_jump = FALSE;


	// Parse the instruction
	opcode = (MEM_WB.IR & 0xFC000000) >> 26;
	function = MEM_WB.IR & 0x0000003F;
	// rs = (MEM_WB.IR & 0x03E00000) >> 21;
	rt = (MEM_WB.IR & 0x001F0000) >> 16;
	rd = (MEM_WB.IR & 0x0000F800) >> 11;
	sa = (MEM_WB.IR & 0x000007C0) >> 6;
	// immediate = MEM_WB.IR & 0x0000FFFF;
	// target = MEM_WB.IR & 0x03FFFFFF;

	if(opcode == 0x00){
		switch(function){
			case 0x00: //SLL
				if (sa != 0){
					NEXT_STATE.REGS[rd] = MEM_WB.ALUOutput;
					CURRENT_STATE.REGS[rd] = NEXT_STATE.REGS[rd];
				}
				break;
			case 0x02: //SRL
			case 0x03: //SRA
				NEXT_STATE.REGS[rd] = MEM_WB.ALUOutput;
				CURRENT_STATE.REGS[rd] = NEXT_STATE.REGS[rd];
				break;

			case 0x08: //JR				
				break;
			case 0x09: //JALR
				NEXT_STATE.REGS[rd] = MEM_WB.ALUOutput;
				CURRENT_STATE.REGS[rd] = NEXT_STATE.REGS[rd];			
				break;

			case 0x0C: //SYSCALL
				if(CURRENT_STATE.REGS[2] == MEM_WB.A){
					RUN_FLAG = FALSE;
				}
				break;
			case 0x10: //MFHI
				//Done
				NEXT_STATE.REGS[rd] = MEM_WB.B;
				CURRENT_STATE.REGS[rd] = NEXT_STATE.REGS[rd];
				break;
			case 0x11: //MTHI
				//Done
				NEXT_STATE.HI = MEM_WB.B;
				break;
			case 0x12: //MFLO
				//Done
				NEXT_STATE.REGS[rd] = MEM_WB.B;
				CURRENT_STATE.REGS[rd] = NEXT_STATE.REGS[rd];
				break;
			case 0x13: //MTLO
				//Done
				NEXT_STATE.LO = MEM_WB.B;
				break;
			case 0x18: //MULT
			case 0x19: //MULTU
			case 0x1A: //DIV
			case 0x1B: //DIVU
				//Done
				NEXT_STATE.HI = MEM_WB.A;
				NEXT_STATE.LO = MEM_WB.ALUOutput;
				break;
			case 0x20: //ADD
			case 0x21: //ADDU
			case 0x22: //SUB
			case 0x23: //SUBU
			case 0x24: //AND
			case 0x25: //OR
			case 0x26: //XOR
			case 0x27: //NOR
			case 0x2A: //SLT
				NEXT_STATE.REGS[rd] = MEM_WB.ALUOutput;
				CURRENT_STATE.REGS[rd] = NEXT_STATE.REGS[rd];
				break;
			default:
				printf("Instruction at 0x%x is not implemented!\n", CURRENT_STATE.PC);
				break;
		}
	}
	else{
		switch(opcode){

			case 0x01:
				if(rt == 0x00000){ //BLTZ
				}
				else if(rt == 0x00001){ //BGEZ
				}
				break;
			case 0x02: //J
			case 0x03: //JAL
			case 0x04: //BEQ
			case 0x05: //BNE				
			case 0x06: //BLEZ				
			case 0x07: //BGTZ
				break;

			case 0x08: //ADDI
			case 0x09: //ADDIU
			case 0x0A: //SLTI
			case 0x0C: //ANDI
			case 0x0D: //ORI
			case 0x0E: //XORI
			case 0x0F: //LUI
				NEXT_STATE.REGS[rt] = MEM_WB.ALUOutput;
				CURRENT_STATE.REGS[rt] = NEXT_STATE.REGS[rt];
				break;
			case 0x20: //LB
			case 0x21: //LH
			case 0x23: //LW
				NEXT_STATE.REGS[rt] = MEM_WB.LMD;
				CURRENT_STATE.REGS[rt] = NEXT_STATE.REGS[rt];
				break;
			case 0x28: //SB
			case 0x29: //SH
			case 0x2B: //SW
				break;
			default:
				// put more things here
				printf("Instruction at 0x%x is not implemented!\n", EX_MEM.PC);
				break;
		}
	}
	if(MEM_WB.IR != 0){
		INSTRUCTION_COUNT += 1;
	}
}

/************************************************************/
/* memory access (MEM) pipeline stage:                                                          */
/************************************************************/
void MEM()
{
	/*IMPLEMENT THIS*/

	// Put variables we need here
	// uint32_t instruction;
	uint32_t opcode, function;
	uint32_t rs, rt, rd;
	uint32_t sa;
	// uint32_t immediate;
	// uint32_t target;
	// uint64_t product, p1, p2;
	// uint32_t addr;
	uint32_t data;
	// int branch_jump = FALSE;

	// Copy program counter and fetched instruction
	MEM_WB.PC = EX_MEM.PC;
	MEM_WB.IR = EX_MEM.IR;

	// Parse the instruction
	opcode = (MEM_WB.IR & 0xFC000000) >> 26;
	function = MEM_WB.IR & 0x0000003F;
	rs = (MEM_WB.IR & 0x03E00000) >> 21;
	rt = (MEM_WB.IR & 0x001F0000) >> 16;
	rd = (MEM_WB.IR & 0x0000F800) >> 11;
	sa = (MEM_WB.IR & 0x000007C0) >> 6;
	// immediate = MEM_WB.IR & 0x0000FFFF;
	// target = MEM_WB.IR & 0x03FFFFFF;


	MEM_WB.RegisterRd = rd;
	MEM_WB.RegisterRt = rt;
	MEM_WB.RegisterRs = rs;

	MEM_WB.RegWrite = 0;

	if(opcode == 0x00){
		switch(function){
			case 0x00: //SLL
				if (sa != 0){
					MEM_WB.RegWrite = 1;
					MEM_WB.RegisterRs = -3;	// -3 means this instruction does not contain this register
					MEM_WB.ALUOutput = EX_MEM.ALUOutput;
				}
				break;
			case 0x02: //SRL
				MEM_WB.RegWrite = 1;
				MEM_WB.RegisterRs = -3;
				MEM_WB.ALUOutput = EX_MEM.ALUOutput;
				break;
			case 0x03: //SRA
				MEM_WB.RegWrite = 1;
				MEM_WB.RegisterRs = -3;
				MEM_WB.ALUOutput = EX_MEM.ALUOutput;
				break;

			case 0x08: //JR				
				break;
			case 0x09: //JALR
				MEM_WB.ALUOutput = EX_MEM.ALUOutput;				
				break;

			case 0x0C: //SYSCALL
				MEM_WB.RegisterRd = -3;
				MEM_WB.RegisterRt = -3;
				MEM_WB.RegisterRs = -3;
				MEM_WB.A = EX_MEM.A;
				break;
			case 0x10: //MFHI
				MEM_WB.RegWrite = 1;
				MEM_WB.RegisterRs = -3;
				MEM_WB.RegisterRt = -3;
				MEM_WB.B = EX_MEM.B;
				break;
			case 0x11: //MTHI
				MEM_WB.RegisterRd = -3;
				MEM_WB.RegisterRt = -3;
				MEM_WB.B = EX_MEM.B;
				break;
			case 0x12: //MFLO
				MEM_WB.RegWrite = 1;
				MEM_WB.RegisterRs = -3;
				MEM_WB.RegisterRt = -3;
				MEM_WB.B = EX_MEM.B;
				break;
			case 0x13: //MTLO
				//Done
				MEM_WB.RegisterRd = -3;
				MEM_WB.RegisterRt = -3;
				MEM_WB.B = EX_MEM.B;
				break;
			case 0x18: //MULT
			case 0x19: //MULTU
			case 0x1A: //DIV
			case 0x1B: //DIVU
				//Done
				MEM_WB.RegisterRd = -3;
				MEM_WB.ALUOutput = EX_MEM.ALUOutput;
				MEM_WB.A = EX_MEM.A;
				break;
			case 0x20: //ADD
			case 0x21: //ADDU
			case 0x22: //SUB
			case 0x23: //SUBU
			case 0x24: //AND
			case 0x25: //OR
			case 0x26: //XOR
			case 0x27: //NOR
			case 0x2A: //SLT
				MEM_WB.RegWrite = 1;
				MEM_WB.ALUOutput = EX_MEM.ALUOutput;
				break;
			default:
				printf("Instruction at 0x%x is not implemented!\n", CURRENT_STATE.PC);
				break;
		}
	}
	else{
		switch(opcode){

			case 0x01:
				if(rt == 0x00000){ //BLTZ
					
				}
				else if(rt == 0x00001){ //BGEZ
					
				}
				break;
			case 0x02: //J	
			case 0x03: //JAL
			case 0x04: //BEQ
			case 0x05: //BNE
			case 0x06: //BLEZ
			case 0x07: //BGTZ
				break;

			case 0x08: //ADDI
			case 0x09: //ADDIU
			case 0x0A: //SLTI
			case 0x0C: //ANDI
			case 0x0D: //ORI
			case 0x0E: //XORI
				MEM_WB.RegWrite = 1;
				MEM_WB.RegisterRd = -3;
				MEM_WB.ALUOutput = EX_MEM.ALUOutput;
				break;
			case 0x0F: //LUI
				MEM_WB.RegWrite = 1;
				MEM_WB.RegisterRd = -3;
				MEM_WB.RegisterRs = -3;
				MEM_WB.ALUOutput = EX_MEM.ALUOutput;
				break;
			case 0x20: //LB
				MEM_WB.RegWrite = 1;
				MEM_WB.RegisterRd = -3;
				data = find_in_cache(EX_MEM.ALUOutput);
				MEM_WB.LMD = ((data & 0x000000FF) & 0x80) > 0 ? (data | 0xFFFFFF00) : (data & 0x000000FF);
				break;
			case 0x21: //LH
				MEM_WB.RegWrite = 1;
				MEM_WB.RegisterRd = -3;
				data = find_in_cache(EX_MEM.ALUOutput);
				MEM_WB.LMD = ((data & 0x0000FFFF) & 0x8000) > 0 ? (data | 0xFFFF0000) : (data & 0x0000FFFF);
				break;
			case 0x23: //LW
				MEM_WB.RegWrite = 1;
				MEM_WB.RegisterRd = -3;
				MEM_WB.LMD = find_in_cache(EX_MEM.ALUOutput);
				break;
			case 0x28: //SB
			case 0x29: //SH
			case 0x2B: //SW Storing in cache!!
				MEM_WB.RegisterRd = -3;
				find_in_cache(EX_MEM.ALUOutput);
				break;
			default:
				// put more things here
				printf("Instruction at 0x%x is not implemented!\n", EX_MEM.PC);
				break;
		}
	}
}

/************************************************************/
/* execution (EX) pipeline stage:                                                                 */
/************************************************************/
void EX()
{
	/*IMPLEMENT THIS*/

	// Put variables we need here
	// uint32_t instruction;
	uint32_t opcode, function;
	uint32_t rs, rt, rd;
	uint32_t sa;
	// uint32_t immediate;
	// uint32_t target;
	uint64_t product, p1, p2, quotient, divident;
	uint32_t addr, data;

	// Copy program counter and fetched instruction
	EX_MEM.PC = IF_EX.PC;
	EX_MEM.IR = IF_EX.IR;

	// Parse the instruction
	opcode = (EX_MEM.IR & 0xFC000000) >> 26;
	function = EX_MEM.IR & 0x0000003F;
	rs = (EX_MEM.IR & 0x03E00000) >> 21;
	rt = (EX_MEM.IR & 0x001F0000) >> 16;
	rd = (EX_MEM.IR & 0x0000F800) >> 11;
	sa = (EX_MEM.IR & 0x000007C0) >> 6;
	//immediate = EX_MEM.IR & 0x0000FFFF;
	//target = EX_MEM.IR & 0x03FFFFFF;	
	EX_MEM.RegisterRd = rd;
	EX_MEM.RegisterRt = rt;
	EX_MEM.RegisterRs = rs;

	EX_MEM.RegWrite = 0;
	EX_MEM.MemRead = 0;
	printf("\n[0x%08x]\t", EX_MEM.PC);
	if(opcode == 0x00){
		switch(function){
			case 0x00: //SLL
				if (sa != 0){
					EX_MEM.RegisterRs = -1;	// -1 means this instruction does not contain this register
					EX_MEM.RegWrite = 1;
					EX_MEM.ALUOutput = IF_EX.B << sa;
					print_instruction(EX_MEM.PC);
				}
				else{
					IF_EX.A = 0;
					IF_EX.B = 0;
					EX_MEM.ALUOutput = IF_EX.B;
				}
				break;
			case 0x02: //SRL
				EX_MEM.RegisterRs = -1;
				EX_MEM.RegWrite = 1;
				EX_MEM.ALUOutput = IF_EX.B >> sa;
				print_instruction(EX_MEM.PC);
				printf("%08x\n",EX_MEM.ALUOutput);
				break;
			case 0x03: //SRA
				EX_MEM.RegisterRs = -1;
				EX_MEM.RegWrite = 1;
				if ((IF_EX.B & 0x80000000) == 1)
				{
					EX_MEM.ALUOutput =  ~(~IF_EX.B >> sa );
				}
				else{
					EX_MEM.ALUOutput = IF_EX.B >> sa;
				}
				print_instruction(EX_MEM.PC);
				break;

			case 0x08: //JR check
				CURRENT_STATE.PC = IF_EX.A;
				//printf("CURRENT_STATE.PC: 0x%08x\t", CURRENT_STATE.PC);
				//print_instruction(CURRENT_STATE.PC);
				BRANCH_FLAG = TRUE;
				TAKEN_FLAG = TRUE;
				print_instruction(EX_MEM.PC);
				break;
			case 0x09: //JALR check
				EX_MEM.ALUOutput = EX_MEM.PC + 4;
				CURRENT_STATE.PC = IF_EX.A;
				//printf("CURRENT_STATE.PC: 0x%08x\t", CURRENT_STATE.PC);
				//print_instruction(CURRENT_STATE.PC);
				BRANCH_FLAG = TRUE;
				TAKEN_FLAG = TRUE;
				print_instruction(EX_MEM.PC);
				break;

			case 0x0C: //SYSCALL
				EX_MEM.RegisterRd = -1;
				EX_MEM.RegisterRt = -1;
				EX_MEM.RegisterRs = -1;
				// if(CURRENT_STATE.REGS[2] == 0xa){
				// 	RUN_FLAG = FALSE;
				// 	print_instruction(EX_MEM.PC);
				// }
				EX_MEM.A = 0xa;
				print_instruction(EX_MEM.PC);
				break;
			case 0x10: //MFHI
				//check
				EX_MEM.RegWrite = 1;
				EX_MEM.RegisterRs = -1;
				EX_MEM.RegisterRt = -1;
				EX_MEM.B = CURRENT_STATE.HI;
				print_instruction(EX_MEM.PC);
				break;
			case 0x11: //MTHI
				//check
				EX_MEM.RegisterRt = -1;
				EX_MEM.RegisterRd = -1;
				EX_MEM.RegWrite = 1;
				EX_MEM.B = IF_EX.A;
				print_instruction(EX_MEM.PC);
				break;
			case 0x12: //MFLO
				//check
				EX_MEM.RegWrite = 1;
				EX_MEM.RegisterRs = -1;
				EX_MEM.RegisterRt = -1;
				EX_MEM.B = CURRENT_STATE.LO;
				print_instruction(EX_MEM.PC);
				break;
			case 0x13: //MTLO
				//check
				EX_MEM.RegisterRt = -1;
				EX_MEM.RegisterRd = -1;
				EX_MEM.RegWrite = 1;
				EX_MEM.B = IF_EX.A;
				print_instruction(EX_MEM.PC);
				break;
			case 0x18: //MULT
				//check
				EX_MEM.RegisterRd = -1;

				if ((IF_EX.A & 0x80000000) == 0x80000000){
					p1 = 0xFFFFFFFF00000000 | IF_EX.A;
				}else{
					p1 = 0x00000000FFFFFFFF & IF_EX.A;
				}
				if ((IF_EX.B & 0x80000000) == 0x80000000){
					p2 = 0xFFFFFFFF00000000 | IF_EX.B;
				}else{
					p2 = 0x00000000FFFFFFFF & IF_EX.B;
				}
				product = (uint64_t)p1 * (uint64_t)p2;
				EX_MEM.ALUOutput = (product & 0X00000000FFFFFFFF);
				EX_MEM.A = (product & 0XFFFFFFFF00000000)>>32;
				print_instruction(EX_MEM.PC);
				break;
			case 0x19: //MULTU
				//check
				EX_MEM.RegisterRd = -1;
				product = (uint64_t)IF_EX.A * (uint64_t)IF_EX.B;
				EX_MEM.ALUOutput = (product & 0X00000000FFFFFFFF);
				EX_MEM.A = (product & 0XFFFFFFFF00000000)>>32;
				print_instruction(EX_MEM.PC);
				break;
			case 0x1A: //DIV
				//check
				EX_MEM.RegisterRd = -1;
				if(IF_EX.B != 0)
				{
					if ((IF_EX.A & 0x80000000) == 0x80000000){
						quotient = 0xFFFFFFFF00000000 | IF_EX.A;
					}else{
						quotient = 0x00000000FFFFFFFF & IF_EX.A;
					}
					if ((IF_EX.B & 0x80000000) == 0x80000000){
						divident = 0xFFFFFFFF00000000 | IF_EX.B;
					}else{
						divident = 0x00000000FFFFFFFF & IF_EX.B;
					}
					EX_MEM.ALUOutput = (int32_t)quotient / (int32_t)divident;
					EX_MEM.A = (int32_t)quotient % (int32_t)divident;
				}
				print_instruction(EX_MEM.PC);
				break;
			case 0x1B: //DIVU
				//check
				EX_MEM.RegisterRd = -1;
				if(IF_EX.B != 0)
				{
					EX_MEM.ALUOutput = (int32_t)IF_EX.A / (int32_t)IF_EX.B;
					EX_MEM.A = (int32_t)IF_EX.A % (int32_t)IF_EX.B;
				}
				print_instruction(EX_MEM.PC);
				break;
			case 0x20: //ADD
				EX_MEM.RegWrite = 1;
				EX_MEM.ALUOutput = IF_EX.A + IF_EX.B;
				print_instruction(EX_MEM.PC);
				break;
			case 0x21: //ADDU
				EX_MEM.RegWrite = 1;
				EX_MEM.ALUOutput = IF_EX.B + IF_EX.A;
				print_instruction(EX_MEM.PC);
				break;
			case 0x22: //SUB
				EX_MEM.RegWrite = 1;
				EX_MEM.ALUOutput = IF_EX.A - IF_EX.B;
				print_instruction(EX_MEM.PC);
				break;
			case 0x23: //SUBU
				EX_MEM.RegWrite = 1;
				EX_MEM.ALUOutput = IF_EX.A - IF_EX.B;
				print_instruction(EX_MEM.PC);
				break;
			case 0x24: //AND
				EX_MEM.RegWrite = 1;
				EX_MEM.ALUOutput = IF_EX.A & IF_EX.B;
				print_instruction(EX_MEM.PC);
				break;
			case 0x25: //OR
				EX_MEM.RegWrite = 1;
				EX_MEM.ALUOutput = IF_EX.A | IF_EX.B;
				print_instruction(EX_MEM.PC);
				break;
			case 0x26: //XOR
				EX_MEM.RegWrite = 1;
				EX_MEM.ALUOutput = IF_EX.A ^ IF_EX.B;
				print_instruction(EX_MEM.PC);
				break;
			case 0x27: //NOR
				EX_MEM.RegWrite = 1;
				EX_MEM.ALUOutput = ~(IF_EX.A | IF_EX.B);
				print_instruction(EX_MEM.PC);
				break;
			case 0x2A: //SLT
				EX_MEM.RegWrite = 1;
				if(IF_EX.A < IF_EX.B){
					EX_MEM.ALUOutput = 0x1;
				}
				else{
					EX_MEM.ALUOutput = 0x0;
				}
				print_instruction(EX_MEM.PC);
				break;
			default:
				printf("Instruction at 0x%x is not implemented!\n", CURRENT_STATE.PC);
				break;
		}
	}
	else{
		switch(opcode){

			case 0x01:
				if(rt == 0x00000){ //BLTZ check
					BRANCH_FLAG = TRUE;
					if((IF_EX.A & 0x80000000) > 0){
						CURRENT_STATE.PC  = EX_MEM.PC + ( (IF_EX.imm & 0x8000) > 0 ? (IF_EX.imm | 0xFFFF0000)<<2 : (IF_EX.imm & 0x0000FFFF)<<2);
					//printf("CURRENT_STATE.PC: 0x%08x\t", CURRENT_STATE.PC);
					//print_instruction(CURRENT_STATE.PC);
						TAKEN_FLAG = TRUE;
					}
					else{
						TAKEN_FLAG = FALSE;
					}
					print_instruction(EX_MEM.PC);
				}
				else if(rt == 0x00001){ //BGEZ check
					BRANCH_FLAG = TRUE;
					if((CURRENT_STATE.REGS[rs] & 0x80000000) == 0x0){
						CURRENT_STATE.PC = EX_MEM.PC + ( (IF_EX.imm & 0x8000) > 0 ? (IF_EX.imm | 0xFFFF0000)<<2 : (IF_EX.imm & 0x0000FFFF)<<2);
						//printf("CURRENT_STATE.PC: 0x%08x\t", CURRENT_STATE.PC);
						//print_instruction(CURRENT_STATE.PC);
						TAKEN_FLAG = TRUE;
					}
					else{
						TAKEN_FLAG = FALSE;
					}
					print_instruction(EX_MEM.PC);
				}
				break;
			case 0x02: //J check
				CURRENT_STATE.PC = (EX_MEM.PC & 0xF0000000) | (IF_EX.target << 2);
				//printf("CURRENT_STATE.PC: 0x%08x\t", CURRENT_STATE.PC);
				//print_instruction(CURRENT_STATE.PC);
				BRANCH_FLAG = TRUE;
				TAKEN_FLAG = TRUE;
				print_instruction(EX_MEM.PC);
				break;
			case 0x03: //JAL check
				CURRENT_STATE.PC = (EX_MEM.PC & 0xF0000000) | (IF_EX.target << 2);
				//printf("CURRENT_STATE.PC: 0x%08x\t", CURRENT_STATE.PC);
				//print_instruction(CURRENT_STATE.PC);
				CURRENT_STATE.REGS[31] = EX_MEM.PC + 4;
				NEXT_STATE.REGS[31] = CURRENT_STATE.REGS[31];
				BRANCH_FLAG = TRUE;
				TAKEN_FLAG = TRUE;
				print_instruction(EX_MEM.PC);
				break;
			case 0x04: //BEQ check
				BRANCH_FLAG = TRUE;
				if(IF_EX.A == IF_EX.B){
					CURRENT_STATE.PC = EX_MEM.PC + ( (IF_EX.imm & 0x8000) > 0 ? (IF_EX.imm | 0xFFFF0000)<<2 : (IF_EX.imm & 0x0000FFFF)<<2);
					//printf("CURRENT_STATE.PC: 0x%08x\t", CURRENT_STATE.PC);
					//print_instruction(CURRENT_STATE.PC);
					TAKEN_FLAG = TRUE;
				}
				else{
					TAKEN_FLAG = FALSE;
				}
				print_instruction(EX_MEM.PC);
				break;
			case 0x05: //BNE check
				BRANCH_FLAG = TRUE;
				if(IF_EX.A != IF_EX.B){
					CURRENT_STATE.PC = EX_MEM.PC + ( (IF_EX.imm & 0x8000) > 0 ? (IF_EX.imm | 0xFFFF0000)<<2 : (IF_EX.imm & 0x0000FFFF)<<2);
					//printf("CURRENT_STATE.PC: 0x%08x\t", CURRENT_STATE.PC);
					//print_instruction(CURRENT_STATE.PC);
					TAKEN_FLAG = TRUE;
				}
				else{
					TAKEN_FLAG = FALSE;
				}
				print_instruction(EX_MEM.PC);
				break;
			case 0x06: //BLEZ check
				BRANCH_FLAG = TRUE;
				if((IF_EX.A & 0x80000000) > 0 || IF_EX.A == 0){
					CURRENT_STATE.PC = EX_MEM.PC +  ( (IF_EX.imm & 0x8000) > 0 ? (IF_EX.imm | 0xFFFF0000)<<2 : (IF_EX.imm & 0x0000FFFF)<<2);
					//printf("CURRENT_STATE.PC: 0x%08x\t", CURRENT_STATE.PC);
					//print_instruction(CURRENT_STATE.PC);
					TAKEN_FLAG = TRUE;
				}
				else{
					TAKEN_FLAG = FALSE;
				}
				print_instruction(EX_MEM.PC);
				break;
			case 0x07: //BGTZ check
				BRANCH_FLAG = TRUE;
				if((IF_EX.A & 0x80000000) == 0x0 || IF_EX.A != 0){
					CURRENT_STATE.PC = EX_MEM.PC +  ( (IF_EX.imm & 0x8000) > 0 ? (IF_EX.imm | 0xFFFF0000)<<2 : (IF_EX.imm & 0x0000FFFF)<<2);
					//printf("CURRENT_STATE.PC: 0x%08x\t", CURRENT_STATE.PC);
					//print_instruction(CURRENT_STATE.PC);
					TAKEN_FLAG = TRUE;
				}
				else{
					TAKEN_FLAG = FALSE;
				}
				print_instruction(EX_MEM.PC);
				break;

			case 0x08: //ADDI
				EX_MEM.RegWrite = 1;
				EX_MEM.RegisterRd = -1;
				EX_MEM.ALUOutput = IF_EX.A + ( (IF_EX.imm & 0x8000) > 0 ? (IF_EX.imm | 0xFFFF0000) : (IF_EX.imm & 0x0000FFFF));
				print_instruction(EX_MEM.PC);
				break;
			case 0x09: //ADDIU
				EX_MEM.RegWrite = 1;
				EX_MEM.RegisterRd = -1;
				EX_MEM.ALUOutput = IF_EX.A + ( (IF_EX.imm & 0x8000) > 0 ? (IF_EX.imm | 0xFFFF0000) : (IF_EX.imm & 0x0000FFFF));
				print_instruction(EX_MEM.PC);
				break;
			case 0x0A: //SLTI
				EX_MEM.RegWrite = 1;
				EX_MEM.RegisterRd = -1;
				if ( (  (int32_t)IF_EX.A - (int32_t)( (IF_EX.imm & 0x8000) > 0 ? (IF_EX.imm | 0xFFFF0000) : (IF_EX.imm & 0x0000FFFF))) < 0){
					EX_MEM.ALUOutput = 0x1;
				}else{
					EX_MEM.ALUOutput = 0x0;
				}
				print_instruction(EX_MEM.PC);
				break;
			case 0x0C: //ANDI
				EX_MEM.RegWrite = 1;
				EX_MEM.RegisterRd = -1;
				EX_MEM.ALUOutput = IF_EX.A & (IF_EX.imm & 0x0000FFFF);
				print_instruction(EX_MEM.PC);
				break;
			case 0x0D: //ORI
				EX_MEM.RegWrite = 1;
				EX_MEM.RegisterRd = -1;
				EX_MEM.ALUOutput = IF_EX.A | (IF_EX.imm & 0x0000FFFF);
				print_instruction(EX_MEM.PC);
				break;
			case 0x0E: //XORI
				EX_MEM.RegWrite = 1;
				EX_MEM.RegisterRd = -1;
				EX_MEM.ALUOutput = IF_EX.A ^ (IF_EX.imm & 0x0000FFFF);
				print_instruction(EX_MEM.PC);
				break;
			case 0x0F: //LUI
				EX_MEM.RegWrite = 1;
				EX_MEM.RegisterRd = -1;
				EX_MEM.RegisterRs = -1;
				EX_MEM.ALUOutput = IF_EX.imm << 16;
				print_instruction(EX_MEM.PC);
				break;
			case 0x20: //LB
				EX_MEM.RegWrite = 1;
				EX_MEM.MemRead = 1;
				EX_MEM.RegisterRd = -1;
				EX_MEM.ALUOutput = IF_EX.A + ( (IF_EX.imm & 0x8000) > 0 ? (IF_EX.imm | 0xFFFF0000) : (IF_EX.imm & 0x0000FFFF));
				print_instruction(EX_MEM.PC);
				break;
			case 0x21: //LH
				EX_MEM.RegWrite = 1;
				EX_MEM.MemRead = 1;
				EX_MEM.RegisterRd = -1;
				EX_MEM.ALUOutput = IF_EX.A + ( (IF_EX.imm & 0x8000) > 0 ? (IF_EX.imm | 0xFFFF0000) : (IF_EX.imm & 0x0000FFFF));
				print_instruction(EX_MEM.PC);
				break;
			case 0x23: //LW
				EX_MEM.RegWrite = 1;
				EX_MEM.MemRead = 1;
				EX_MEM.RegisterRd = -1;
				EX_MEM.ALUOutput = IF_EX.A + ( (IF_EX.imm & 0x8000) > 0 ? (IF_EX.imm | 0xFFFF0000) : (IF_EX.imm & 0x0000FFFF));
				print_instruction(EX_MEM.PC);
				break;
			case 0x28: //SB
				EX_MEM.RegisterRd = -1;
				addr = IF_EX.A + ( (IF_EX.imm & 0x8000) > 0 ? (IF_EX.imm | 0xFFFF0000) : (IF_EX.imm & 0x0000FFFF));
				EX_MEM.ALUOutput = addr;
				data = mem_read_32( addr);
				EX_MEM.B = (data & 0xFFFFFF00) | (IF_EX.B & 0x000000FF);
				//mem_write_32(addr, data);
				print_instruction(EX_MEM.PC);
				break;
			case 0x29: //SH
				EX_MEM.RegisterRd = -1;
				addr = IF_EX.A + ( (IF_EX.imm & 0x8000) > 0 ? (IF_EX.imm | 0xFFFF0000) : (IF_EX.imm & 0x0000FFFF));
				EX_MEM.ALUOutput = addr;
				data = mem_read_32( addr);
				EX_MEM.B = (data & 0xFFFF0000) | (IF_EX.B & 0x0000FFFF);
				//mem_write_32(addr, data);
				print_instruction(EX_MEM.PC);
				break;
			case 0x2B: //SW
				EX_MEM.RegisterRd = -1;
				addr = IF_EX.A + ( (IF_EX.imm & 0x8000) > 0 ? (IF_EX.imm | 0xFFFF0000) : (IF_EX.imm & 0x0000FFFF));
				EX_MEM.ALUOutput = addr;
				EX_MEM.B = IF_EX.B;
				//mem_write_32(addr, CURRENT_STATE.REGS[rt]);
				print_instruction(EX_MEM.PC);
				break;
			default:
				// put more things here
				printf("Instruction at 0x%x is not implemented!\n", EX_MEM.PC);
				break;
		}
	}

}

/************************************************************/
/* instruction decode (ID) pipeline stage:                                                         */
/************************************************************/
void ID()
{
	/*IMPLEMENT THIS*/
	if(BRANCH_FLAG == TRUE){
		IF_EX.PC =  0x00000000;
		IF_EX.IR =  0x00000000;
		IF_EX.A = 0;
		IF_EX.B = 0;
		IF_EX.imm = 0;
		printf("\t\tBranch Stall");
		return;
	}

	if(TAKEN_FLAG == TRUE){
		IF_EX.PC =  0x00000000;
		IF_EX.IR =  0x00000000;
		IF_EX.A = 0;
		IF_EX.B = 0;
		IF_EX.imm = 0;
		TAKEN_FLAG = FALSE;
		printf("Flush");
		return;
	}

	// Copy program counter and fetched instruction
	IF_EX.PC = ID_IF.PC;
	IF_EX.IR = ID_IF.IR;

	uint32_t opcode, function;
	uint32_t rs, rt, rd;
	uint32_t sa;
	uint32_t immediate;
	// Get rs and rt from the instruction for the n+1 instruction
	opcode = (IF_EX.IR & 0xFC000000) >> 26;
	function = IF_EX.IR & 0x0000003F;
	rs = (IF_EX.IR & 0x03E00000) >> 21;
	rt = (IF_EX.IR & 0x001F0000) >> 16;
	rd = (IF_EX.IR & 0x0000F800) >> 11;
	sa = (IF_EX.IR & 0x000007C0) >> 6;
	IF_EX.A = CURRENT_STATE.REGS[rs];		// get content in RS
	IF_EX.B = CURRENT_STATE.REGS[rt];		// get content in RT
	IF_EX.target = IF_EX.IR & 0x03FFFFFF;

	// Get immediate
	immediate = IF_EX.IR & 0x0000FFFF;
	uint32_t mask = 0x00008000;
	if((immediate & mask) != 0){
		IF_EX.imm = 0xFFFF0000 | immediate;
	}
	else{
		IF_EX.imm = immediate;
	}

	IF_EX.RegisterRs = rs;
	IF_EX.RegisterRt = rt;
	IF_EX.RegisterRd = rd;

	if(opcode == 0x00){
		switch(function){
			case 0x00: //SLL
				if (sa != 0){
					IF_EX.RegisterRs = -2;	// -2 means this instruction does not contain this register
				}
				else{
					// Do nothing here
				}
				break;
			case 0x02: //SRL
				IF_EX.RegisterRs = -2;
				break;
			case 0x03: //SRA
				IF_EX.RegisterRs = -2;
				break;
			case 0x08: //JR
				IF_EX.RegisterRd = -2;
				IF_EX.RegisterRt = -2;
				break;
			case 0x09: //JALR
				IF_EX.RegisterRt = -2;
				break;
			case 0x0C: //SYSCALL
				IF_EX.RegisterRd = -2;
				IF_EX.RegisterRt = -2;
				IF_EX.RegisterRs = -2;
				break;
			case 0x10: //MFHI
				//check
				IF_EX.RegisterRs = -2;
				IF_EX.RegisterRt = -2;
				break;
			case 0x11: //MTHI
				//check
				IF_EX.RegisterRt = -2;
				IF_EX.RegisterRd = -2;
				break;
			case 0x12: //MFLO
				//check
				IF_EX.RegisterRs = -2;
				IF_EX.RegisterRt = -2;
				break;
			case 0x13: //MTLO
				//check
				IF_EX.RegisterRt = -2;
				IF_EX.RegisterRd = -2;
				break;
			case 0x18: //MULT
				//check
				IF_EX.RegisterRd = -2;
				break;
			case 0x19: //MULTU
				//check
				IF_EX.RegisterRd = -2;
				break;
			case 0x1A: //DIV
				//check
				IF_EX.RegisterRd = -2;
				break;
			case 0x1B: //DIVU
				//check
				IF_EX.RegisterRd = -2;
				break;
			case 0x20: //ADD
				break;
			case 0x21: //ADDU
				break;
			case 0x22: //SUB
				break;
			case 0x23: //SUBU
				break;
			case 0x24: //AND
				break;
			case 0x25: //OR
				break;
			case 0x26: //XOR
				break;
			case 0x27: //NOR

				break;
			case 0x2A: //SLT
				break;
			default:
				printf("Instruction at 0x%x is not implemented!\n", CURRENT_STATE.PC);
				break;
		}
//EX Forwarding

		if(ENABLE_FORWARDING && EX_MEM.RegWrite && EX_MEM.MemRead == 0 && EX_MEM.RegisterRd==-1 && EX_MEM.RegisterRt==IF_EX.RegisterRs){
			IF_EX.A = EX_MEM.ALUOutput;
		}
		else if(ENABLE_FORWARDING && EX_MEM.RegWrite && EX_MEM.MemRead == 0 && EX_MEM.RegisterRd==-1 && EX_MEM.RegisterRt==IF_EX.RegisterRt){
			IF_EX.B = EX_MEM.ALUOutput;
		}
		else if(ENABLE_FORWARDING && EX_MEM.RegWrite && EX_MEM.RegisterRd!=0 && EX_MEM.RegisterRd==IF_EX.RegisterRs){
			IF_EX.A = EX_MEM.ALUOutput;
		
		}
		else if(ENABLE_FORWARDING && EX_MEM.RegWrite && EX_MEM.RegisterRd!=0 && EX_MEM.RegisterRd==IF_EX.RegisterRt){
			IF_EX.B = EX_MEM.ALUOutput;
		}

		
//Detecting EX hazards in R-type instructions
		else if(EX_MEM.RegWrite && EX_MEM.RegisterRd!=0 && EX_MEM.RegisterRd==IF_EX.RegisterRs){
			IF_EX.IR = 0x00000000;
			CURRENT_STATE.PC = CURRENT_STATE.PC - 0x04;
			IF_EX.PC = IF_EX.PC - 0x04;
			IF_EX.A = 0;
			IF_EX.B = 0;
			IF_EX.imm = 0;
			printf("EX_MEM.RegisterRd==IF_EX.RegisterRs :RAW(Detecting EX hazard in R-type instruction)\n");
		}
		else if(EX_MEM.RegWrite && EX_MEM.RegisterRd!=0 && EX_MEM.RegisterRd==IF_EX.RegisterRt){
			IF_EX.IR = 0x00000000;
			CURRENT_STATE.PC = CURRENT_STATE.PC - 0x04;
			IF_EX.PC = IF_EX.PC - 0x04;
			IF_EX.A = 0;
			IF_EX.B = 0;
			IF_EX.imm = 0;
			printf("EX_MEM.RegisterRd==IF_EX.RegisterRt :RAW(Detecting EX hazard in R-type instruction)\n");
		}
//Detecting EX hazards when I-type instruction is before R-type
		else if(EX_MEM.RegWrite && EX_MEM.RegisterRd ==-1 && EX_MEM.RegisterRt==IF_EX.RegisterRs){
			IF_EX.IR = 0x00000000;
			CURRENT_STATE.PC = CURRENT_STATE.PC - 0x04;
			IF_EX.PC = IF_EX.PC - 0x04;
			IF_EX.A = 0;
			IF_EX.B = 0;
			IF_EX.imm = 0;
			printf("EX_MEM.RegisterRt==IF_EX.RegisterRs :RAW(Detecting EX hazard when I-type instruction is before R-typedxcfgvzdxcv)\n");
		}
		else if(EX_MEM.RegWrite && EX_MEM.RegisterRd ==-1 && EX_MEM.RegisterRt==IF_EX.RegisterRt){
			IF_EX.IR = 0x00000000;
			CURRENT_STATE.PC = CURRENT_STATE.PC - 0x04;
			IF_EX.PC = IF_EX.PC - 0x04;
			IF_EX.A = 0;
			IF_EX.B = 0;
			IF_EX.imm = 0;
			printf("EX_MEM.RegisterRt==IF_EX.RegisterRt :RAW(Detecting EX hazard when I-type instruction is before R-type)\n");
		}
//MEM Forwarding
		else if(ENABLE_FORWARDING && MEM_WB.RegWrite && MEM_WB.RegisterRd!=0 && MEM_WB.RegisterRd==IF_EX.RegisterRs){
			if(MEM_WB.LMD !=0)
				IF_EX.A = MEM_WB.LMD;
			else if(MEM_WB.ALUOutput !=0)
				IF_EX.A = MEM_WB.ALUOutput;
			else
				IF_EX.A = 0;
			}
		else if(ENABLE_FORWARDING && MEM_WB.RegWrite && MEM_WB.RegisterRd!=0 && MEM_WB.RegisterRd==IF_EX.RegisterRt){
			if(MEM_WB.LMD !=0)
				IF_EX.B = MEM_WB.LMD;
			else if(MEM_WB.ALUOutput !=0)
				IF_EX.B = MEM_WB.ALUOutput;
			else
				IF_EX.B = 0;
			}
		else if(ENABLE_FORWARDING && MEM_WB.RegWrite && MEM_WB.RegisterRd!=0 && MEM_WB.RegisterRt==IF_EX.RegisterRs){
			if(MEM_WB.LMD !=0)
				IF_EX.A = MEM_WB.LMD;
			else if(MEM_WB.ALUOutput !=0)
				IF_EX.A = MEM_WB.ALUOutput;
			else
				IF_EX.A = 0;
			}
		else if(ENABLE_FORWARDING && MEM_WB.RegWrite && MEM_WB.RegisterRd!=0 && MEM_WB.RegisterRt==IF_EX.RegisterRt){
			if(MEM_WB.LMD !=0)
				IF_EX.B = MEM_WB.LMD;
			else if(MEM_WB.ALUOutput !=0)
				IF_EX.B = MEM_WB.ALUOutput;
			else
				IF_EX.B = 0;
			}

//Detecting MEM hazards in R-type instructions	
		else if(MEM_WB.RegWrite && MEM_WB.RegisterRd!=0 && MEM_WB.RegisterRd==IF_EX.RegisterRs){
			IF_EX.IR = 0x00000000;
			CURRENT_STATE.PC = CURRENT_STATE.PC - 0x04;
			IF_EX.PC = IF_EX.PC - 0x04;
			IF_EX.A = 0;
			IF_EX.B = 0;
			IF_EX.imm = 0;
			printf("MEM_WB.RegisterRd==IF_EX.RegisterRs (Detecting MEM hazard in R-type instruction)\n");
		}
		else if(MEM_WB.RegWrite && MEM_WB.RegisterRd!=0 && MEM_WB.RegisterRd==IF_EX.RegisterRt){
			IF_EX.IR = 0x00000000;
			CURRENT_STATE.PC = CURRENT_STATE.PC - 0x04;
			IF_EX.PC = IF_EX.PC - 0x04;
			IF_EX.A = 0;
			IF_EX.B = 0;
			IF_EX.imm = 0;
			printf("MEM_WB.RegisterRd==IF_EX.RegisterRt (Detecting MEM hazard in R-type instruction)\n");
		}
//Detecting MEM hazards when I-type instruction is before R-type
		else if(MEM_WB.RegWrite && MEM_WB.RegisterRd!=0 && MEM_WB.RegisterRt==IF_EX.RegisterRs){
			IF_EX.IR = 0x00000000;
			CURRENT_STATE.PC = CURRENT_STATE.PC - 0x04;
			IF_EX.PC = IF_EX.PC - 0x04;
			IF_EX.A = 0;
			IF_EX.B = 0;
			IF_EX.imm = 0;
			printf("EX_MEM.RegisterRt==IF_EX.RegisterRs (Detecting MEM hazard when I-type instruction is before R-type)\n");
		}
		else if(MEM_WB.RegWrite && MEM_WB.RegisterRd!=0 && MEM_WB.RegisterRt==IF_EX.RegisterRt){
			IF_EX.IR = 0x00000000;
			CURRENT_STATE.PC = CURRENT_STATE.PC - 0x04;
			IF_EX.PC = IF_EX.PC - 0x04;
			IF_EX.A = 0;
			IF_EX.B = 0;
			IF_EX.imm = 0;
			printf("EX_MEM.RegisterRt==IF_EX.RegisterRt (Detecting MEM hazard when I-type instruction is before R-type)\n");
		}
	}
	else{
		switch(opcode){

			case 0x01:
				if(rt == 0x00000){ //BLTZ
					IF_EX.RegisterRd = -2;
					IF_EX.RegisterRt = -2;
				}
				else if(rt == 0x00001){ //BGEZ
					IF_EX.RegisterRd = -2;
					IF_EX.RegisterRt = -2;
				}
				break;
			case 0x02: //J
				IF_EX.RegisterRd = -2;
				IF_EX.RegisterRt = -2;
				IF_EX.RegisterRs = -2;
				break;
			case 0x03: //JAL
				IF_EX.RegisterRd = -2;
				IF_EX.RegisterRt = -2;
				IF_EX.RegisterRs = -2;
				break;
			case 0x04: //BEQ
				IF_EX.RegisterRd = -2;
				break;
			case 0x05: //BNE
				IF_EX.RegisterRd = -2;
				break;
			case 0x06: //BLEZ
				IF_EX.RegisterRd = -2;
				IF_EX.RegisterRt = -2;
				break;
			case 0x07: //BGTZ
				IF_EX.RegisterRd = -2;
				IF_EX.RegisterRt = -2;
				break;
			case 0x08: //ADDI
				IF_EX.RegisterRd = -2;
				break;
			case 0x09: //ADDIU
				IF_EX.RegisterRd = -2;
				break;
			case 0x0A: //SLTI
				IF_EX.RegisterRd = -2;
				break;
			case 0x0C: //ANDI
				IF_EX.RegisterRd = -2;
				break;
			case 0x0D: //ORI
				IF_EX.RegisterRd = -2;
				break;
			case 0x0E: //XORI
				IF_EX.RegisterRd = -2;
				break;
			case 0x0F: //LUI
				IF_EX.RegisterRd = -2;
				IF_EX.RegisterRs = -2;
				break;
			case 0x20: //LB
				IF_EX.RegisterRd = -2;
				break;
			case 0x21: //LH
				IF_EX.RegisterRd = -2;
				break;
			case 0x23: //LW
				IF_EX.RegisterRd = -2;
				break;
			case 0x28: //SB
				IF_EX.RegisterRd = -2;
				break;
			case 0x29: //SH
				IF_EX.RegisterRd = -2;
				break;
			case 0x2B: //SW
				IF_EX.RegisterRd = -2;
				break;
			default:
				// put more things here
				printf("Instruction at 0x%x is not implemented!\n", EX_MEM.PC);
				break;
		}


		if(ENABLE_FORWARDING && EX_MEM.RegWrite && EX_MEM.RegisterRd!=0 && EX_MEM.RegisterRt==IF_EX.RegisterRs){
			IF_EX.A = EX_MEM.ALUOutput;
		}
		else if(ENABLE_FORWARDING && EX_MEM.RegWrite && EX_MEM.RegisterRd!=0 && EX_MEM.RegisterRt==IF_EX.RegisterRt){
			IF_EX.B = EX_MEM.ALUOutput;
		}
		else if(ENABLE_FORWARDING && EX_MEM.RegWrite && EX_MEM.RegisterRd!=0 && EX_MEM.RegisterRd==IF_EX.RegisterRt){
			IF_EX.B = EX_MEM.ALUOutput;
		}
		else if(ENABLE_FORWARDING && EX_MEM.RegWrite && EX_MEM.RegisterRd!=0 && EX_MEM.RegisterRd==IF_EX.RegisterRs){
			IF_EX.A = EX_MEM.ALUOutput;
			printf("%d\n",EX_MEM.ALUOutput);
		}

//Detecting EX hazards in I-type instructions
		else if(EX_MEM.RegWrite && EX_MEM.RegisterRd!=0 && EX_MEM.RegisterRt==IF_EX.RegisterRs){
			IF_EX.IR = 0x00000000;
			CURRENT_STATE.PC = CURRENT_STATE.PC - 0x04;
			IF_EX.PC = IF_EX.PC - 0x04;
			IF_EX.A = 0;
			IF_EX.B = 0;
			IF_EX.imm = 0;
			printf("EX_MEM.RegisterRt==IF_EX.RegisterRs :RAW (Detecting EX hazard in I-type instruction)\n");
		}
		else if(EX_MEM.RegWrite && EX_MEM.RegisterRd!=0 && EX_MEM.RegisterRt==IF_EX.RegisterRt){
			IF_EX.IR = 0x00000000;
			CURRENT_STATE.PC = CURRENT_STATE.PC - 0x04;
			IF_EX.PC = IF_EX.PC - 0x04;
			IF_EX.A = 0;
			IF_EX.B = 0;
			IF_EX.imm = 0;
			printf("EX_MEM.RegisterRt==IF_EX.RegisterRt :WAW (Detecting EX hazard in I-type instruction)\n");
		}
//Detecting EX hazards when R-type instruction is before I-type
		else if(EX_MEM.RegWrite && EX_MEM.RegisterRd!=0 && EX_MEM.RegisterRd==IF_EX.RegisterRt){
			IF_EX.IR = 0x00000000;
			CURRENT_STATE.PC = CURRENT_STATE.PC - 0x04;
			IF_EX.PC = IF_EX.PC - 0x04;
			IF_EX.A = 0;
			IF_EX.B = 0;
			IF_EX.imm = 0;
			printf("EX_MEM.RegisterRd==IF_EX.RegisterRt :WAW(Detecting EX hazard when R-type instruction is before I-type)\n");
		}
		else if(EX_MEM.RegWrite && EX_MEM.RegisterRd!=0 && EX_MEM.RegisterRd==IF_EX.RegisterRs){
			IF_EX.IR = 0x00000000;
			CURRENT_STATE.PC = CURRENT_STATE.PC - 0x04;
			IF_EX.PC = IF_EX.PC - 0x04;
			IF_EX.A = 0;
			IF_EX.B = 0;
			IF_EX.imm = 0;
			printf("EX_MEM.RegisterRd==IF_EX.RegisterRs :RAW(Detecting EX hazard when R-type instruction is before I-type)\n");
		}
//Mem forwarding in i type hazards
		else if(ENABLE_FORWARDING && MEM_WB.RegWrite && MEM_WB.RegisterRd!=0 && MEM_WB.RegisterRt==IF_EX.RegisterRs){
			if(MEM_WB.MemRead == 1)
				IF_EX.A = MEM_WB.LMD;
			else if(MEM_WB.ALUOutput !=0)
				IF_EX.A = MEM_WB.ALUOutput;
			else
				IF_EX.A = 0;
		}
		else if(ENABLE_FORWARDING && MEM_WB.RegWrite && MEM_WB.RegisterRd!=0 && MEM_WB.RegisterRt==IF_EX.RegisterRt){
			if(MEM_WB.MemRead == 1)
				IF_EX.B = MEM_WB.LMD;
			else if(MEM_WB.ALUOutput !=0)
				IF_EX.B = MEM_WB.ALUOutput;			
		}

		else if(ENABLE_FORWARDING && MEM_WB.RegWrite && MEM_WB.RegisterRd!=0 && MEM_WB.RegisterRd==IF_EX.RegisterRt){
			if(MEM_WB.MemRead == 1)
				IF_EX.B = MEM_WB.LMD;
			else if(MEM_WB.ALUOutput !=0)
				IF_EX.B = MEM_WB.ALUOutput;
		}
		else if(ENABLE_FORWARDING && MEM_WB.RegWrite && MEM_WB.RegisterRd!=0 && MEM_WB.RegisterRd==IF_EX.RegisterRs){
			if(MEM_WB.MemRead == 1)
				IF_EX.A = MEM_WB.LMD;
			else if(MEM_WB.ALUOutput !=0)
				IF_EX.A = MEM_WB.ALUOutput;
		}



//Detecting MEM hazards in I-type instructions		
		else if(MEM_WB.RegWrite && MEM_WB.RegisterRd!=0 && MEM_WB.RegisterRt==IF_EX.RegisterRs){
			IF_EX.IR = 0x00000000;
			CURRENT_STATE.PC = CURRENT_STATE.PC - 0x04;
			IF_EX.PC = IF_EX.PC - 0x04;
			IF_EX.A = 0;
			IF_EX.B = 0;
			IF_EX.imm = 0;
			printf("MEM_WB.RegisterRt==IF_EX.RegisterRs (Detecting MEM hazard in I-type instruction)\n");
		}
		else if(MEM_WB.RegWrite && MEM_WB.RegisterRd!=0 && MEM_WB.RegisterRt==IF_EX.RegisterRt){
			IF_EX.IR = 0x00000000;
			CURRENT_STATE.PC = CURRENT_STATE.PC - 0x04;
			IF_EX.PC = IF_EX.PC - 0x04;
			IF_EX.A = 0;
			IF_EX.B = 0;
			IF_EX.imm = 0;
			printf("MEM_WB.RegisterRt==IF_EX.RegisterRt (Detecting MEM hazard in I-type instruction)\n");
		}
//Detecting EX hazards when R-type instruction is before I-type
		else if(MEM_WB.RegWrite && MEM_WB.RegisterRd!=0 && MEM_WB.RegisterRd==IF_EX.RegisterRt){
			IF_EX.IR = 0x00000000;
			CURRENT_STATE.PC = CURRENT_STATE.PC - 0x04;
			IF_EX.PC = IF_EX.PC - 0x04;
			IF_EX.A = 0;
			IF_EX.B = 0;
			IF_EX.imm = 0;
			printf("EX_MEM.RegisterRd==IF_EX.RegisterRt (Detecting MEM hazard when R-type instruction is before I-type)\n");
		}
		else if(MEM_WB.RegWrite && MEM_WB.RegisterRd!=0 && MEM_WB.RegisterRd==IF_EX.RegisterRs){
			IF_EX.IR = 0x00000000;
			CURRENT_STATE.PC = CURRENT_STATE.PC - 0x04;
			IF_EX.PC = IF_EX.PC - 0x04;
			IF_EX.A = 0;
			IF_EX.B = 0;
			IF_EX.imm = 0;
			printf("EX_MEM.RegisterRd==IF_EX.RegisterRs (Detecting MEM hazard when R-type instruction is before I-type)\n");
		}
	}
}

/************************************************************/
/* instruction fetch (IF) pipeline stage:                                                              */
/************************************************************/
void IF()
{
	/*IMPLEMENT THIS*/
	if(BRANCH_FLAG == TRUE && TAKEN_FLAG == TRUE){
		BRANCH_FLAG = FALSE;
		NEXT_STATE.PC = CURRENT_STATE.PC;
		//printf("BRANCH_FLAG = FALSE\n");
		return;	
	}
	else if(BRANCH_FLAG == TRUE && TAKEN_FLAG == FALSE){
		BRANCH_FLAG = FALSE;
		//printf("BRANCH_FLAG = FALSE\n");
		return;	
	}

	ID_IF.PC = CURRENT_STATE.PC;
	ID_IF.IR = mem_read_32(ID_IF.PC);
	NEXT_STATE.PC = CURRENT_STATE.PC + 0x04;
}


/************************************************************/
/* Initialize Memory                                                                                                    */
/************************************************************/
void initialize() {
	init_memory();
	init_cache();
	CURRENT_STATE.PC = MEM_TEXT_BEGIN;
	NEXT_STATE = CURRENT_STATE;
	RUN_FLAG = TRUE;
}

/************************************************************/
/* Print the program loaded into memory (in MIPS assembly format)    */
/************************************************************/
void print_program(){
	/*IMPLEMENT THIS*/
	int i;
	uint32_t pc;

	printf("\n");
	for(i=0; i<PROGRAM_SIZE; i++){
		pc = MEM_REGIONS[0].begin + (0x04 * i);
		printf("[0x%08x]\t", pc);
		print_instruction(pc);
	}
	printf("\n");
}

/************************************************************/
/* Print the current pipeline                                                                                    */
/************************************************************/
void show_pipeline(){
	/*IMPLEMENT THIS*/

		printf("\n\n");

		printf("\nIF Stage\n");
		
		printf("Current PC: %08x\n", ID_IF.PC);
		printf("IF_ID.IR: %08x\n", ID_IF.IR);
		printf("\t");
		print_instruction(ID_IF.PC);
		printf("IF_ID.PC: %08x\n\n", CURRENT_STATE.PC);
		printf("\nID Stage\n");
		printf("ID_EX.IR: %08x\n", IF_EX.IR);
		printf("\t");
		if(IF_EX.IR == 0){
			printf("Bubble\n");
			
		}
		else
			print_instruction(IF_EX.PC);
		printf("ID_EX.A: %08x\n", IF_EX.A);
		printf("ID_EX.B: %08x\n", IF_EX.B);
		printf("ID_EX.imm: %08x\n\n", IF_EX.imm);

		printf("\nEX Stage\n");
		printf("EX_MEM.IR: %08x\n", EX_MEM.IR);
		printf("\t");
		if(EX_MEM.IR == 0){
			printf("Bubble\n");
		}
		else
			print_instruction(EX_MEM.PC);
		printf("EX_MEM.A: %08x\n", EX_MEM.A);
		printf("EX_MEM.B: %08x\n", EX_MEM.B);
		printf("EX_MEM.ALUOutput: %08x\n\n", EX_MEM.ALUOutput);

		printf("\nMEM Stage\n");
		printf("MEM_WB.IR: %08x\n", MEM_WB.IR);
		printf("\t");
		if(MEM_WB.IR == 0){
			printf("Bubble\n");
		}
		else
			print_instruction(MEM_WB.PC);
		printf("MEM_WB.ALUOutput: %08x\n", MEM_WB.ALUOutput);
		printf("MEM_WB.LMD: %08x\n\n", MEM_WB.LMD);

		printf("\n");
}

// Get this from professor's code
/************************************************************/
/* Print the instruction at given memory address (in MIPS assembly format)    */
/************************************************************/
void print_instruction(uint32_t addr){
	uint32_t instruction, opcode, function, rs, rt, rd, sa, immediate, target;
	
	instruction = mem_read_32(addr);	
	opcode = (instruction & 0xFC000000) >> 26;
	function = instruction & 0x0000003F;
	rs = (instruction & 0x03E00000) >> 21;
	rt = (instruction & 0x001F0000) >> 16;
	rd = (instruction & 0x0000F800) >> 11;
	sa = (instruction & 0x000007C0) >> 6;
	immediate = instruction & 0x0000FFFF;
	target = instruction & 0x03FFFFFF;

	if(opcode == 0x00){
		/*R format instructions here*/

		switch(function){
			case 0x00:
				printf("SLL $r%u, $r%u, 0x%x\n", rd, rt, sa);
				break;
			case 0x02:
				printf("SRL $r%u, $r%u, 0x%x\n", rd, rt, sa);
				break;
			case 0x03:
				printf("SRA $r%u, $r%u, 0x%x\n", rd, rt, sa);
				break;
			case 0x08:
				printf("JR $r%u\n", rs);
				break;
			case 0x09:
				if(rd == 31){
					printf("JALR $r%u\n", rs);
				}
				else{
					printf("JALR $r%u, $r%u\n", rd, rs);
				}
				break;
			case 0x0C:
				printf("SYSCALL\n");
				break;
			case 0x10:
				printf("MFHI $r%u\n", rd);
				break;
			case 0x11:
				printf("MTHI $r%u\n", rs);
				break;
			case 0x12:
				printf("MFLO $r%u\n", rd);
				break;
			case 0x13:
				printf("MTLO $r%u\n", rs);
				break;
			case 0x18:
				printf("MULT $r%u, $r%u\n", rs, rt);
				break;
			case 0x19:
				printf("MULTU $r%u, $r%u\n", rs, rt);
				break;
			case 0x1A:
				printf("DIV $r%u, $r%u\n", rs, rt);
				break;
			case 0x1B:
				printf("DIVU $r%u, $r%u\n", rs, rt);
				break;
			case 0x20:
				printf("ADD $r%u, $r%u, $r%u\n", rd, rs, rt);
				break;
			case 0x21:
				printf("ADDU $r%u, $r%u, $r%u\n", rd, rs, rt);
				break;
			case 0x22:
				printf("SUB $r%u, $r%u, $r%u\n", rd, rs, rt);
				break;
			case 0x23:
				printf("SUBU $r%u, $r%u, $r%u\n", rd, rs, rt);
				break;
			case 0x24:
				printf("AND $r%u, $r%u, $r%u\n", rd, rs, rt);
				break;
			case 0x25:
				printf("OR $r%u, $r%u, $r%u\n", rd, rs, rt);
				break;
			case 0x26:
				printf("XOR $r%u, $r%u, $r%u\n", rd, rs, rt);
				break;
			case 0x27:
				printf("NOR $r%u, $r%u, $r%u\n", rd, rs, rt);
				break;
			case 0x2A:
				printf("SLT $r%u, $r%u, $r%u\n", rd, rs, rt);
				break;
			default:
				printf("Instruction is not implemented!\n");
				break;
		}
	}
	else{
		switch(opcode){
			case 0x01:
				if(rt == 0){
					printf("BLTZ $r%u, 0x%x\n", rs, immediate<<2);
				}
				else if(rt == 1){
					printf("BGEZ $r%u, 0x%x\n", rs, immediate<<2);
				}
				break;
			case 0x02:
				printf("J 0x%x\n", (addr & 0xF0000000) | (target<<2));
				break;
			case 0x03:
				printf("JAL 0x%x\n", (addr & 0xF0000000) | (target<<2));
				break;
			case 0x04:
				printf("BEQ $r%u, $r%u, 0x%x\n", rs, rt, immediate<<2);
				break;
			case 0x05:
				printf("BNE $r%u, $r%u, 0x%x\n", rs, rt, immediate<<2);
				break;
			case 0x06:
				printf("BLEZ $r%u, 0x%x\n", rs, immediate<<2);
				break;
			case 0x07:
				printf("BGTZ $r%u, 0x%x\n", rs, immediate<<2);
				break;
			case 0x08:
				printf("ADDI $r%u, $r%u, 0x%x\n", rt, rs, immediate);
				break;
			case 0x09:
				printf("ADDIU $r%u, $r%u, 0x%x\n", rt, rs, immediate);
				break;
			case 0x0A:
				printf("SLTI $r%u, $r%u, 0x%x\n", rt, rs, immediate);
				break;
			case 0x0C:
				printf("ANDI $r%u, $r%u, 0x%x\n", rt, rs, immediate);
				break;
			case 0x0D:
				printf("ORI $r%u, $r%u, 0x%x\n", rt, rs, immediate);
				break;
			case 0x0E:
				printf("XORI $r%u, $r%u, 0x%x\n", rt, rs, immediate);
				break;
			case 0x0F:
				printf("LUI $r%u, 0x%x\n", rt, immediate);
				break;
			case 0x20:
				printf("LB $r%u, 0x%x($r%u)\n", rt, immediate, rs);
				break;
			case 0x21:
				printf("LH $r%u, 0x%x($r%u)\n", rt, immediate, rs);
				break;
			case 0x23:
				printf("LW $r%u, 0x%x($r%u)\n", rt, immediate, rs);
				break;
			case 0x28:
				printf("SB $r%u, 0x%x($r%u)\n", rt, immediate, rs);
				break;
			case 0x29:
				printf("SH $r%u, 0x%x($r%u)\n", rt, immediate, rs);
				break;
			case 0x2B:
				printf("SW $r%u, 0x%x($r%u)\n", rt, immediate, rs);
				break;
			default:
				printf("Instruction is not implemented!\n");
				break;
		}
	}
}

/***************************************************************/
/* main                                                                                                                                   */
/***************************************************************/
int main(int argc, char *argv[]) {
	printf("\n**************************\n");
	printf("Welcome to MU-MIPS SIM...\n");
	printf("**************************\n\n");

	if (argc < 2) {
		printf("Error: You should provide input file.\nUsage: %s <input program> \n\n",  argv[0]);
		exit(1);
	}

	strcpy(prog_file, argv[1]);
	initialize();
	load_program();
	help();
	while (1){
		handle_command();
	}
	return 0;
}
