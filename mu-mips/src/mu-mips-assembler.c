#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <ctype.h>



uint32_t RegisterNumber(char registerName[]) {

	uint32_t regNum = 0;

		if(strcmp(registerName, "$zero") == 0){
			regNum = 0;
		}
		else if(strcmp(registerName, "$at") == 0){
			regNum = 1;
		}
		else if(strcmp(registerName, "$v0") == 0){
			regNum = 2;
		}
		else if(strcmp(registerName, "$v1") == 0){
			regNum = 3;
		}
		else if(strcmp(registerName, "$a0") == 0){
			regNum = 4;
		}
		else if(strcmp(registerName, "$a1") == 0){
			regNum = 5;
		}
		else if(strcmp(registerName, "$a2") == 0){
			regNum = 6;
		}
		else if(strcmp(registerName, "$a3") == 0){
			regNum = 7;
		}
		else if(strcmp(registerName, "$t0") == 0){
			regNum = 8;
		}
		else if(strcmp(registerName, "$t1") == 0){
			regNum = 9;
		}
		else if(strcmp(registerName, "$t2") == 0){
			regNum = 10;
		}
		else if(strcmp(registerName, "$t3") == 0){
			regNum = 11;
		}
		else if(strcmp(registerName, "$t4") == 0){
			regNum = 12;
		}
		else if(strcmp(registerName, "$t5") == 0){
			regNum = 13;
		}
		else if(strcmp(registerName, "$t6") == 0){
			regNum = 14;
		}
		else if(strcmp(registerName, "$t7") == 0){
			regNum = 15;
		}
		else if(strcmp(registerName, "$s0") == 0){
			regNum = 16;
		}
		else if(strcmp(registerName, "$s1") == 0){
			regNum = 17;
		}
		else if(strcmp(registerName, "$s2") == 0){
			regNum = 18;
		}
		else if(strcmp(registerName, "$s3") == 0){
			regNum = 19;
		}
		else if(strcmp(registerName, "$s4") == 0){
			regNum = 20;
		}
		else if(strcmp(registerName, "$s5") == 0){
			regNum = 21;
		}
		else if(strcmp(registerName, "$s6") == 0){
			regNum = 22;
		}
		else if(strcmp(registerName, "$s7") == 0){
			regNum = 23;
		}
		else if(strcmp(registerName, "$t8") == 0){
			regNum = 24;
		}
		else if(strcmp(registerName, "$t9") == 0){
			regNum = 25;
		}
		else if(strcmp(registerName, "$k0") == 0){
			regNum = 26;
		}
		else if(strcmp(registerName, "$k1") == 0){
			regNum = 27;
		}
		else if(strcmp(registerName, "$gp") == 0){
			regNum = 28;
		}
		else if(strcmp(registerName, "$sp") == 0){
			regNum = 29;
		}
		else if(strcmp(registerName, "$fp") == 0){
			regNum = 30;
		}
		else if(strcmp(registerName, "$ra") == 0){
			regNum = 31;
		}
		else{
			printf("REGISTER ERROR");
			exit(-1);
		}

  return regNum;
}


uint32_t machine_code(char assembly_instruction[]) {

	uint32_t machineCode = 0;
	uint32_t rs, rt, rd, shamt, offset, immediate, target, base;
	char *temp_instruction;
	char *partial_instruction[6];
	char *temp;
	int count = 0;
	int i = 0;

	temp_instruction = malloc(strlen(assembly_instruction)*sizeof(char));
	strncpy(temp_instruction, assembly_instruction, strlen(assembly_instruction));

	temp = strtok(temp_instruction, " ");

	while(temp != NULL){

		partial_instruction[count] = temp;

		for(i=0 ; i<strlen(partial_instruction[count]); i++) {
			if(isalpha(partial_instruction[count][i])){
				partial_instruction[count][i] = tolower(partial_instruction[count][i]);
			}
		}

		//printf("%d: %s\n", count, partial_instruction[count]);
		count++;
		temp = strtok(NULL, ", ()");
	}

	i=0;
	count=0;

	if(strcmp(partial_instruction[0], "add") == 0){
		// done
		machineCode = 0x00000020;
		rs = RegisterNumber(partial_instruction[2]) << 21;
		rt = RegisterNumber(partial_instruction[3]) << 16;
		rd = RegisterNumber(partial_instruction[1]) << 11;
		machineCode = machineCode | rs;
		machineCode = machineCode | rt;
		machineCode = machineCode | rd;
	}
	else if(strcmp(partial_instruction[0], "addu") == 0){
		// done
		machineCode = 0x00000021;
		rs = RegisterNumber(partial_instruction[2]) << 21;
		rt = RegisterNumber(partial_instruction[3]) << 16;
		rd = RegisterNumber(partial_instruction[1]) << 11;
		machineCode = machineCode | rs;
		machineCode = machineCode | rt;
		machineCode = machineCode | rd;
	}
	else if(strcmp(partial_instruction[0], "addi") == 0){
		// done
		machineCode = 0x20000000;
		rs = RegisterNumber(partial_instruction[2]) << 21;
		rt = RegisterNumber(partial_instruction[1]) << 16;
		immediate = (uint32_t)strtol(partial_instruction[3], NULL, 16);
		immediate = immediate & 0x0000FFFF;
		machineCode = machineCode | rs;
		machineCode = machineCode | rt;
		machineCode = machineCode | immediate;
	}
	else if(strcmp(partial_instruction[0], "addiu") == 0){
		//done
		machineCode = 0x24000000;
		rs = RegisterNumber(partial_instruction[2]) << 21;
		rt = RegisterNumber(partial_instruction[1]) << 16;
		immediate = (uint32_t)strtol(partial_instruction[3], NULL, 16);
		immediate = immediate & 0x0000FFFF;
		machineCode = machineCode | rs;
		machineCode = machineCode | rt;
		machineCode = machineCode | immediate;
	}
	else if(strcmp(partial_instruction[0], "sub") == 0){
		// done
		machineCode = 0x00000022;
		rs = RegisterNumber(partial_instruction[2]) << 21;
		rt = RegisterNumber(partial_instruction[3]) << 16;
		rd = RegisterNumber(partial_instruction[1]) << 11;
		machineCode = machineCode | rs;
		machineCode = machineCode | rt;
		machineCode = machineCode | rd;
	}
	else if(strcmp(partial_instruction[0], "subu") == 0){
		// done
		machineCode = 0x00000023;
		rs = RegisterNumber(partial_instruction[2]) << 21;
		rt = RegisterNumber(partial_instruction[3]) << 16;
		rd = RegisterNumber(partial_instruction[1]) << 11;
		machineCode = machineCode | rs;
		machineCode = machineCode | rt;
		machineCode = machineCode | rd;
	}
	else if(strcmp(partial_instruction[0], "mult") == 0){
		// done
		machineCode = 0x00000018;
		rs = RegisterNumber(partial_instruction[1]) << 21;
		rt = RegisterNumber(partial_instruction[2]) << 16;
		machineCode = machineCode | rs;
		machineCode = machineCode | rt;
	}
	else if(strcmp(partial_instruction[0], "multu") == 0){
		// done
		machineCode = 0x00000019;
		rs = RegisterNumber(partial_instruction[1]) << 21;
		rt = RegisterNumber(partial_instruction[2]) << 16;
		machineCode = machineCode | rs;
		machineCode = machineCode | rt;
	}
	else if(strcmp(partial_instruction[0], "div") == 0){
		// done
		machineCode = 0x0000001A;
		rs = RegisterNumber(partial_instruction[1]) << 21;
		rt = RegisterNumber(partial_instruction[2]) << 16;
		machineCode = machineCode | rs;
		machineCode = machineCode | rt;
	}
	else if(strcmp(partial_instruction[0], "divu") == 0){
		// done
		machineCode = 0x0000001B;
		rs = RegisterNumber(partial_instruction[1]) << 21;
		rt = RegisterNumber(partial_instruction[2]) << 16;
		machineCode = machineCode | rs;
		machineCode = machineCode | rt;
	}
	else if(strcmp(partial_instruction[0], "and") == 0){
		// done
		machineCode = 0x00000024;
		rs = RegisterNumber(partial_instruction[2]) << 21;
		rt = RegisterNumber(partial_instruction[3]) << 16;
		rd = RegisterNumber(partial_instruction[1]) << 11;
		machineCode = machineCode | rs;
		machineCode = machineCode | rt;
		machineCode = machineCode | rd;
	}
	else if(strcmp(partial_instruction[0], "andi") == 0){
		//done
		machineCode = 0x30000000;
		rs = RegisterNumber(partial_instruction[2]) << 21;
		rt = RegisterNumber(partial_instruction[1]) << 16;
		immediate = (uint32_t)strtol(partial_instruction[3], NULL, 16);
		immediate = immediate & 0x0000FFFF;
		machineCode = machineCode | rs;
		machineCode = machineCode | rt;
		machineCode = machineCode | immediate;
	}
	else if(strcmp(partial_instruction[0], "or") == 0){
		// done
		machineCode = 0x00000025;
		rs = RegisterNumber(partial_instruction[2]) << 21;
		rt = RegisterNumber(partial_instruction[3]) << 16;
		rd = RegisterNumber(partial_instruction[1]) << 11;
		machineCode = machineCode | rs;
		machineCode = machineCode | rt;
		machineCode = machineCode | rd;
	}
	else if(strcmp(partial_instruction[0], "ori") == 0){
		// done
		machineCode = 0x34000000;
		rs = RegisterNumber(partial_instruction[2]) << 21;
		rt = RegisterNumber(partial_instruction[1]) << 16;
		immediate = (uint32_t)strtol(partial_instruction[3], NULL, 16);
		immediate = immediate & 0x0000FFFF;
		machineCode = machineCode | rs;
		machineCode = machineCode | rt;
		machineCode = machineCode | immediate;
	}
	else if(strcmp(partial_instruction[0], "xor") == 0){
		// done
		machineCode = 0x00000026;
		rs = RegisterNumber(partial_instruction[2]) << 21;
		rt = RegisterNumber(partial_instruction[3]) << 16;
		rd = RegisterNumber(partial_instruction[1]) << 11;
		machineCode = machineCode | rs;
		machineCode = machineCode | rt;
		machineCode = machineCode | rd;
	}
	else if(strcmp(partial_instruction[0], "xori") == 0){
		// done
		machineCode = 0x38000000;
		rs = RegisterNumber(partial_instruction[2]) << 21;
		rt = RegisterNumber(partial_instruction[1]) << 16;
		immediate = (uint32_t)strtol(partial_instruction[3], NULL, 16);
		immediate = immediate & 0x0000FFFF;
		machineCode = machineCode | rs;
		machineCode = machineCode | rt;
		machineCode = machineCode | immediate;
	}
	else if(strcmp(partial_instruction[0], "nor") == 0){
		// done
		machineCode = 0x00000027;
		rs = RegisterNumber(partial_instruction[2]) << 21;
		rt = RegisterNumber(partial_instruction[3]) << 16;
		rd = RegisterNumber(partial_instruction[1]) << 11;
		machineCode = machineCode | rs;
		machineCode = machineCode | rt;
		machineCode = machineCode | rd;
	}
	else if(strcmp(partial_instruction[0], "slt") == 0){
		// done
		machineCode = 0x0000002A;
		rs = RegisterNumber(partial_instruction[2]) << 21;
		rt = RegisterNumber(partial_instruction[3]) << 16;
		rd = RegisterNumber(partial_instruction[1]) << 11;
		machineCode = machineCode | rs;
		machineCode = machineCode | rt;
		machineCode = machineCode | rd;
	}
	else if(strcmp(partial_instruction[0], "slti") == 0){
		// done
		machineCode = 0x28000000;
		rs = RegisterNumber(partial_instruction[2]) << 21;
		rt = RegisterNumber(partial_instruction[1]) << 16;
		immediate = (uint32_t)strtol(partial_instruction[3], NULL, 16);
		immediate = immediate & 0x0000FFFF;
		machineCode = machineCode | rs;
		machineCode = machineCode | rt;
		machineCode = machineCode | immediate;
	}
	else if(strcmp(partial_instruction[0], "sll") == 0){
		// done
		machineCode = 0x00000000;
		rs = RegisterNumber(partial_instruction[2]) << 16;
		rt = RegisterNumber(partial_instruction[1]) << 11;
		shamt = (uint32_t)strtol(partial_instruction[3], NULL, 16);
		shamt = shamt << 6;
		machineCode = machineCode | rs;
		machineCode = machineCode | rt;
		machineCode = machineCode | shamt;
	}
	else if(strcmp(partial_instruction[0], "srl") == 0){
		// done
		machineCode = 0x00000002;
		rs = RegisterNumber(partial_instruction[2]) << 16;
		rt = RegisterNumber(partial_instruction[1]) << 11;
		shamt = (uint32_t)strtol(partial_instruction[3], NULL, 16);
		shamt = shamt << 6;
		machineCode = machineCode | rs;
		machineCode = machineCode | rt;
		machineCode = machineCode | shamt;
	}
	else if(strcmp(partial_instruction[0], "sra") == 0){
		// done
		machineCode = 0x00000003;
		rs = RegisterNumber(partial_instruction[2]) << 16;
		rt = RegisterNumber(partial_instruction[1]) << 11;
		shamt = (uint32_t)strtol(partial_instruction[3], NULL, 16);
		shamt = shamt << 6;
		machineCode = machineCode | rs;
		machineCode = machineCode | rt;
		machineCode = machineCode | shamt;
	}
	else if(strcmp(partial_instruction[0], "lui") == 0){
		// done
		machineCode = 0x3C000000;
		rt = RegisterNumber(partial_instruction[1]) << 16;
		immediate = (uint32_t)strtol(partial_instruction[2], NULL, 16);
		immediate = immediate & 0x0000FFFF;
		machineCode = machineCode | rt;
		machineCode = machineCode | immediate;
	}
	else if(strcmp(partial_instruction[0], "lw") == 0){
		// done
		machineCode = 0x8C000000;
		base = RegisterNumber(partial_instruction[3]) << 21;
		rt = RegisterNumber(partial_instruction[1]) << 16;
		offset = (uint32_t)strtol(partial_instruction[2], NULL, 16);
		offset = offset & 0x0000FFFF;
		machineCode = machineCode | base;
		machineCode = machineCode | rt;
		machineCode = machineCode | offset;
	}
	else if(strcmp(partial_instruction[0], "lb") == 0){
		// done
		machineCode = 0x80000000;
		base = RegisterNumber(partial_instruction[3]) << 21;
		rt = RegisterNumber(partial_instruction[1]) << 16;
		offset = (uint32_t)strtol(partial_instruction[2], NULL, 16);
		offset = offset & 0x0000FFFF;
		machineCode = machineCode | base;
		machineCode = machineCode | rt;
		machineCode = machineCode | offset;
	}
	else if(strcmp(partial_instruction[0], "lh") == 0){
		// done
		machineCode = 0x84000000;
		base = RegisterNumber(partial_instruction[3]) << 21;
		rt = RegisterNumber(partial_instruction[1]) << 16;
		offset = (uint32_t)strtol(partial_instruction[2], NULL, 16);
		offset = offset & 0x0000FFFF;
		machineCode = machineCode | base;
		machineCode = machineCode | rt;
		machineCode = machineCode | offset;
	}
	else if(strcmp(partial_instruction[0], "sw") == 0){
		// done
		machineCode = 0xAC000000;
		base = RegisterNumber(partial_instruction[3]) << 21;
		rt = RegisterNumber(partial_instruction[1]) << 16;
		offset = (uint32_t)strtol(partial_instruction[2], NULL, 16);
		offset = offset & 0x0000FFFF;
		machineCode = machineCode | base;
		machineCode = machineCode | rt;
		machineCode = machineCode | offset;
	}
	else if(strcmp(partial_instruction[0], "sb") == 0){
		// done
		machineCode = 0xA0000000;
		base = RegisterNumber(partial_instruction[3]) << 21;
		rt = RegisterNumber(partial_instruction[1]) << 16;
		offset = (uint32_t)strtol(partial_instruction[2], NULL, 16);
		offset = offset & 0x0000FFFF;
		machineCode = machineCode | base;
		machineCode = machineCode | rt;
		machineCode = machineCode | offset;
	}
	else if(strcmp(partial_instruction[0], "sh") == 0){
		// done
		machineCode = 0xA4000000;
		base = RegisterNumber(partial_instruction[3]) << 21;
		rt = RegisterNumber(partial_instruction[1]) << 16;
		offset = (uint32_t)strtol(partial_instruction[2], NULL, 16);
		offset = offset & 0x0000FFFF;
		machineCode = machineCode | base;
		machineCode = machineCode | rt;
		machineCode = machineCode | offset;
	}
	else if(strcmp(partial_instruction[0], "mfhi") == 0){
		// done
		machineCode = 0x00000010;
		rd = RegisterNumber(partial_instruction[1]) << 11;
		machineCode = machineCode | rd;
	}
	else if(strcmp(partial_instruction[0], "mflo") == 0){
		// done
		machineCode = 0x00000012;
		rd = RegisterNumber(partial_instruction[1]) << 11;
		machineCode = machineCode | rd;
	}
	else if(strcmp(partial_instruction[0], "mthi") == 0){
		// done
		machineCode = 0x00000011;
		rs = RegisterNumber(partial_instruction[1]) << 21;
		machineCode = machineCode | rs;
	}
	else if(strcmp(partial_instruction[0], "mtlo") == 0){
		// done
		machineCode = 0x00000013;
		rs = RegisterNumber(partial_instruction[1]) << 21;
		machineCode = machineCode | rs;
	}
	else if(strcmp(partial_instruction[0], "beq") == 0){
		// done
		machineCode = 0x10000000;
		rs = RegisterNumber(partial_instruction[1]) << 21;
		rt = RegisterNumber(partial_instruction[2]) << 16;
		offset = (uint32_t)strtol(partial_instruction[3], NULL, 16);
		offset = offset & 0x0000FFFF;
		machineCode = machineCode | rs;
		machineCode = machineCode | rt;
		machineCode = machineCode | offset;
	}
	else if(strcmp(partial_instruction[0], "bne") == 0){
		// done
		machineCode = 0x14000000;
		rs = RegisterNumber(partial_instruction[1]) << 21;
		rt = RegisterNumber(partial_instruction[2]) << 16;
		offset = (uint32_t)strtol(partial_instruction[3], NULL, 16);
		offset = offset & 0x0000FFFF;
		machineCode = machineCode | rs;
		machineCode = machineCode | rt;
		machineCode = machineCode | offset;
	}
	else if(strcmp(partial_instruction[0], "blez") == 0){
		// done
		machineCode = 0x18000000;
		rs = RegisterNumber(partial_instruction[1]) << 21;
		offset = (uint32_t)strtol(partial_instruction[2], NULL, 16);
		offset = offset & 0x0000FFFF;
		machineCode = machineCode | rs;
		machineCode = machineCode | offset;
	}
	else if(strcmp(partial_instruction[0], "bltz") == 0){
		// done
		machineCode = 0x04000000;
		rs = RegisterNumber(partial_instruction[1]) << 21;
		offset = (uint32_t)strtol(partial_instruction[2], NULL, 16);
		offset = offset & 0x0000FFFF;
		machineCode = machineCode | rs;
		machineCode = machineCode | offset;
	}
	else if(strcmp(partial_instruction[0], "bgez") == 0){
		// done
		machineCode = 0x04010000;
		rs = RegisterNumber(partial_instruction[1]) << 21;
		offset = (uint32_t)strtol(partial_instruction[2], NULL, 16);
		offset = offset & 0x0000FFFF;
		machineCode = machineCode | rs;
		machineCode = machineCode | offset;
	}
	else if(strcmp(partial_instruction[0], "bgtz") == 0){
		// done
		machineCode = 0x1C000000;
		rs = RegisterNumber(partial_instruction[1]) << 21;
		offset = (uint32_t)strtol(partial_instruction[2], NULL, 16);
		offset = offset & 0x0000FFFF;
		machineCode = machineCode | rs;
		machineCode = machineCode | offset;
	}
	else if(strcmp(partial_instruction[0], "j") == 0){
		// done
		machineCode = 0x08000000;
		target = (uint32_t)strtol(partial_instruction[1], NULL, 16);
		target = target & 0x03FFFFFF;
		machineCode = machineCode | target;
	}
	else if(strcmp(partial_instruction[0], "jr") == 0){
		// done
		machineCode = 0x00000008;
		rs = RegisterNumber(partial_instruction[1]) << 21;
		machineCode = machineCode | rs;
	}
	else if(strcmp(partial_instruction[0], "jal") == 0){
		// done
		machineCode = 0x0C000000;
		target = (uint32_t)strtol(partial_instruction[1], NULL, 16);
		target = target & 0x03FFFFFF;
		machineCode = machineCode | target;
	}
	else if(strcmp(partial_instruction[0], "jalr") == 0){
		// check
		machineCode = 0x00000009;
			rs = RegisterNumber(partial_instruction[2]) << 21;
			rd = RegisterNumber(partial_instruction[1]) << 11;
			machineCode = machineCode | rs;
			machineCode = machineCode | rd;
	}
	else if(strcmp(partial_instruction[0], "syscall") == 0){
		machineCode = 0x0000000C;
	}
	else{
		printf("INSTRUCTION ERROR\n");
		exit(-1);
	}

	return machineCode;

}



int main(int argc, char *argv[])
{
  FILE *fp;	// read the assembly code from the file where this file pointer points to
	FILE *wf;	// write the machine code to the file where this file pointer points to

  char assembly_prog_file[100];
  char save_machine_code_file[100];
  char buffer[255];
	char instruction[255];
	char *comment;
	uint32_t assemblyMachineCode;

  if (argc != 3){
		printf("Invalid arguement!!!");
		exit(0);
	}

  sprintf (assembly_prog_file, "../inputs/%s", argv[1]);
  sprintf (save_machine_code_file, "../outputs/%s", argv[2]);

	// open files for reading and writing
  fp = fopen(assembly_prog_file, "r");
	wf = fopen(save_machine_code_file, "w");
	printf("\n");

  while(fgets(buffer, sizeof(buffer), (FILE*) fp)){

		// remove comment in this if-else statement
		if(strstr(buffer, "//")){
			comment = strstr(buffer, "//");
			strncpy(instruction, buffer, strlen(buffer)-strlen(comment));
			instruction[strlen(buffer)-strlen(comment)] = '\0';
    }
		else{
			strncpy(instruction, buffer, strlen(buffer));
			instruction[strlen(buffer)] = '\0';
			instruction[strlen(buffer)-1] = '\0';
		}

		// see instruction
		printf("%s\n", instruction);

		// generate and show machine code
		assemblyMachineCode = machine_code(instruction);
		printf("%08x\n", assemblyMachineCode);

		// write the machine code into a file
		sprintf (buffer, "%08x\n", assemblyMachineCode);
		fputs(buffer, wf);
  }

  fclose(fp);
	fclose(wf);

  return 0;
}
