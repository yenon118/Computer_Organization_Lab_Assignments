#! /bin/sh


make


### just type .in file name and .out filename from inputs and outputs directory
./mu-mips-assembler bubble_sort.in bubble_sort.out
#./mu-mips-assembler sumto10.in sumto10.out
#./mu-mips-assembler testall.in testall.out
#./mu-mips-assembler other_sort.in other_sort.out
#./mu-mips-assembler fibonacci.in fibonacci.out


### run mu-mips here (only one file can be run at one time)
./mu-mips ../outputs/bubble_sort.out
#./mu-mips ../outputs/sumto10.out
#./mu-mips ../outputs/testall.out
#./mu-mips ../outputs/other_sort.out
#./mu-mips ../outputs/fibonacci.out
