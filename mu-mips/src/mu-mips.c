#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>

#include "mu-mips.h"

/***************************************************************/
/* Print out a list of commands available                                                                  */
/***************************************************************/
void help() {
	printf("------------------------------------------------------------------\n\n");
	printf("\t**********MU-MIPS Help MENU**********\n\n");
	printf("sim\t-- simulate program to completion \n");
	printf("run <n>\t-- simulate program for <n> instructions\n");
	printf("rdump\t-- dump register values\n");
	printf("reset\t-- clears all registers/memory and re-loads the program\n");
	printf("input <reg> <val>\t-- set GPR <reg> to <val>\n");
	printf("mdump <start> <stop>\t-- dump memory from <start> to <stop> address\n");
	printf("high <val>\t-- set the HI register to <val>\n");
	printf("low <val>\t-- set the LO register to <val>\n");
	printf("print\t-- print the program loaded into memory\n");
	printf("?\t-- display help menu\n");
	printf("quit\t-- exit the simulator\n\n");
	printf("------------------------------------------------------------------\n\n");
}

/***************************************************************/
/* Read a 32-bit word from memory                                                                            */
/***************************************************************/
uint32_t mem_read_32(uint32_t address)
{
	int i;
	for (i = 0; i < NUM_MEM_REGION; i++) {
		if ( (address >= MEM_REGIONS[i].begin) &&  ( address <= MEM_REGIONS[i].end) ) {
			uint32_t offset = address - MEM_REGIONS[i].begin;
			return (MEM_REGIONS[i].mem[offset+3] << 24) |
					(MEM_REGIONS[i].mem[offset+2] << 16) |
					(MEM_REGIONS[i].mem[offset+1] <<  8) |
					(MEM_REGIONS[i].mem[offset+0] <<  0);
		}
	}
	return 0;
}

/***************************************************************/
/* Write a 32-bit word to memory                                                                                */
/***************************************************************/
void mem_write_32(uint32_t address, uint32_t value)
{
	int i;
	uint32_t offset;
	for (i = 0; i < NUM_MEM_REGION; i++) {
		if ( (address >= MEM_REGIONS[i].begin) && (address <= MEM_REGIONS[i].end) ) {
			offset = address - MEM_REGIONS[i].begin;

			MEM_REGIONS[i].mem[offset+3] = (value >> 24) & 0xFF;
			MEM_REGIONS[i].mem[offset+2] = (value >> 16) & 0xFF;
			MEM_REGIONS[i].mem[offset+1] = (value >>  8) & 0xFF;
			MEM_REGIONS[i].mem[offset+0] = (value >>  0) & 0xFF;
		}
	}
}

/***************************************************************/
/* Execute one cycle                                                                                                              */
/***************************************************************/
void cycle() {
	handle_instruction();
	CURRENT_STATE = NEXT_STATE;
	INSTRUCTION_COUNT++;
}

/***************************************************************/
/* Simulate MIPS for n cycles                                                                                       */
/***************************************************************/
void run(int num_cycles) {

	if (RUN_FLAG == FALSE) {
		printf("Simulation Stopped\n\n");
		return;
	}

	printf("Running simulator for %d cycles...\n\n", num_cycles);
	int i;
	for (i = 0; i < num_cycles; i++) {
		if (RUN_FLAG == FALSE) {
			printf("Simulation Stopped.\n\n");
			break;
		}
		cycle();
	}
}

/***************************************************************/
/* simulate to completion                                                                                               */
/***************************************************************/
void runAll() {
	if (RUN_FLAG == FALSE) {
		printf("Simulation Stopped.\n\n");
		return;
	}

	printf("Simulation Started...\n\n");
	while (RUN_FLAG){
		cycle();
	}
	printf("\n\nSimulation Finished.\n\n");
}

/***************************************************************/
/* Dump a word-aligned region of memory to the terminal                              */
/***************************************************************/
void mdump(uint32_t start, uint32_t stop) {
	uint32_t address;

	printf("-------------------------------------------------------------\n");
	printf("Memory content [0x%08x..0x%08x] :\n", start, stop);
	printf("-------------------------------------------------------------\n");
	printf("\t[Address in Hex (Dec) ]\t[Value]\n");
	for (address = start; address <= stop; address += 4){
		printf("\t0x%08x (%d) :\t0x%08x\n", address, address, mem_read_32(address));
	}
	printf("\n");
}

/***************************************************************/
/* Dump current values of registers to the teminal                                              */
/***************************************************************/
void rdump() {
	int i;
	printf("-------------------------------------\n");
	printf("Dumping Register Content\n");
	printf("-------------------------------------\n");
	printf("# Instructions Executed\t: %u\n", INSTRUCTION_COUNT);
	printf("PC\t: 0x%08x\n", CURRENT_STATE.PC);
	printf("-------------------------------------\n");
	printf("[Register]\t[Value]\n");
	printf("-------------------------------------\n");
	for (i = 0; i < MIPS_REGS; i++){
		printf("[R%d]\t: 0x%08x\n", i, CURRENT_STATE.REGS[i]);
	}
	printf("-------------------------------------\n");
	printf("[HI]\t: 0x%08x\n", CURRENT_STATE.HI);
	printf("[LO]\t: 0x%08x\n", CURRENT_STATE.LO);
	printf("-------------------------------------\n");
}

/***************************************************************/
/* Read a command from standard input.                                                               */
/***************************************************************/
void handle_command() {
	char buffer[20];
	uint32_t start, stop, cycles;
	uint32_t register_no;
	int register_value;
	int hi_reg_value, lo_reg_value;

	printf("\nMU-MIPS SIM:> ");

	if (scanf("%s", buffer) == EOF){
		exit(0);
	}

	switch(buffer[0]) {
	case 'S':
	case 's':
		runAll();
		break;
	case 'M':
	case 'm':
		if (scanf("%x %x", &start, &stop) != 2){
			break;
		}
		mdump(start, stop);
		break;
	case '?':
		help();
		break;
	case 'Q':
	case 'q':
		printf("**************************\n");
		printf("Exiting MU-MIPS! Good Bye...\n");
		printf("**************************\n");
		exit(0);
	case 'R':
	case 'r':
		if (buffer[1] == 'd' || buffer[1] == 'D'){
			rdump();
		}else if(buffer[1] == 'e' || buffer[1] == 'E'){
			reset();
		}
		else {
			if (scanf("%d", &cycles) != 1) {
				break;
			}
			run(cycles);
		}
		break;
	case 'I':
	case 'i':
		if (scanf("%u %i", &register_no, &register_value) != 2){
			break;
		}
		CURRENT_STATE.REGS[register_no] = register_value;
		NEXT_STATE.REGS[register_no] = register_value;
		break;
	case 'H':
	case 'h':
		if (scanf("%i", &hi_reg_value) != 1){
			break;
		}
		CURRENT_STATE.HI = hi_reg_value;
		NEXT_STATE.HI = hi_reg_value;
		break;
	case 'L':
	case 'l':
		if (scanf("%i", &lo_reg_value) != 1){
			break;
		}
		CURRENT_STATE.LO = lo_reg_value;
		NEXT_STATE.LO = lo_reg_value;
		break;
	case 'P':
	case 'p':
		print_program();
		break;
	default:
		printf("Invalid Command.\n");
		break;
	}
}

/***************************************************************/
/* reset registers/memory and reload program                                                    */
/***************************************************************/
void reset() {
	int i;
	/*reset registers*/
	for (i = 0; i < MIPS_REGS; i++){
		CURRENT_STATE.REGS[i] = 0;
	}
	CURRENT_STATE.HI = 0;
	CURRENT_STATE.LO = 0;

	for (i = 0; i < NUM_MEM_REGION; i++) {
		uint32_t region_size = MEM_REGIONS[i].end - MEM_REGIONS[i].begin + 1;
		memset(MEM_REGIONS[i].mem, 0, region_size);
	}

	/*load program*/
	load_program();

	/*reset PC*/
	INSTRUCTION_COUNT = 0;
	CURRENT_STATE.PC =  MEM_TEXT_BEGIN;
	NEXT_STATE = CURRENT_STATE;
	RUN_FLAG = TRUE;
}

/***************************************************************/
/* Allocate and set memory to zero                                                                            */
/***************************************************************/
void init_memory() {
	int i;
	for (i = 0; i < NUM_MEM_REGION; i++) {
		uint32_t region_size = MEM_REGIONS[i].end - MEM_REGIONS[i].begin + 1;
		MEM_REGIONS[i].mem = malloc(region_size);
		memset(MEM_REGIONS[i].mem, 0, region_size);
	}
}

/**************************************************************/
/* load program into memory                                                                                      */
/**************************************************************/
void load_program() {
	FILE * fp;
	int i, word;
	uint32_t address;

	/* Open program file. */
	fp = fopen(prog_file, "r");
	if (fp == NULL) {
		printf("Error: Can't open program file %s\n", prog_file);
		exit(-1);
	}

	/* Read in the program. */

	i = 0;
	while( fscanf(fp, "%x\n", &word) != EOF ) {
		address = MEM_TEXT_BEGIN + i;
		mem_write_32(address, word);
		printf("writing 0x%08x into address 0x%08x (%d)\n", word, address, address);
		i += 4;
	}
	PROGRAM_SIZE = i/4;
	printf("Program loaded into memory.\n%d words written into memory.\n\n", PROGRAM_SIZE);
	fclose(fp);
}

/************************************************************/
/* decode and execute instruction                                                                     */
/************************************************************/
void handle_instruction()
{
	/*IMPLEMENT THIS*/
	/* execute one instruction at a time. Use/update CURRENT_STATE and and NEXT_STATE, as necessary.*/
	//int i=0;
	uint32_t instruction, opcode, function, rs, rt, rd, sa, offset_immediate, target;
	uint64_t product;

	if(INSTRUCTION_COUNT == PROGRAM_SIZE){
		RUN_FLAG = FALSE;
	}
	else{

		instruction = mem_read_32(MEM_REGIONS[0].begin + (0x04 * INSTRUCTION_COUNT));

		opcode = (instruction & 0xFC000000) >> 26;
		function = instruction & 0x0000003F;
		rs = (instruction & 0x03E00000) >> 21;
		rt = (instruction & 0x001F0000) >> 16;
		rd = (instruction & 0x0000F800) >> 11;
		sa = (instruction & 0x000007C0) >> 6;
		offset_immediate = instruction & 0x0000FFFF;
		target = instruction & 0x03FFFFFF;

		if(opcode == 0x00){
			/*R format instructions here*/

			switch(function){
			case 0x00:
				// check
				if (sa != 0){
					NEXT_STATE.REGS[rd] = CURRENT_STATE.REGS[rt] << sa;
					printf("\nSLL Instruction");
				}
				else{
				}
				break;
			case 0x02:
				// done
				NEXT_STATE.REGS[rd] = CURRENT_STATE.REGS[rt] >> sa;
				printf("\nSRL Instruction");
				break;
			case 0x03:
				// done
				if ((CURRENT_STATE.REGS[rt] & 0x80000000) > 0)
				{
					NEXT_STATE.REGS[rd] = ~CURRENT_STATE.REGS[rt];
					NEXT_STATE.REGS[rd] = NEXT_STATE.REGS[rd] >> sa;
					NEXT_STATE.REGS[rd] = ~NEXT_STATE.REGS[rd];

				}
				else{
					NEXT_STATE.REGS[rd] = CURRENT_STATE.REGS[rt] >> sa;
				}
				printf("\nSRA Instruction");
				break;
			case 0x08:
				// check
				NEXT_STATE.PC = CURRENT_STATE.REGS[rs];
				printf("\nJR Instruction");
				break;
			case 0x09:
				// check
				NEXT_STATE.REGS[rd] = CURRENT_STATE.PC + 0x08;
				NEXT_STATE.PC = CURRENT_STATE.REGS[rs];
				printf("\nJALR Instruction");
				break;
			case 0x0C:
				// check
				printf("\nSYSCALL Instruction\n");
				if(CURRENT_STATE.REGS[2] == 10){
					RUN_FLAG = FALSE;
					//exit(0);
				}
				break;
			case 0x10:
				// check
				NEXT_STATE.REGS[rd] = CURRENT_STATE.HI;
				printf("\nMFHI Instruction");
				break;
			case 0x11:
				// check
				NEXT_STATE.HI = CURRENT_STATE.REGS[rs];
				printf("\nMTHI Instruction");
				break;
			case 0x12:
				// check
				NEXT_STATE.REGS[rd] = CURRENT_STATE.LO;
				printf("\nMFLO Instruction");
				break;
			case 0x13:
				// check
				NEXT_STATE.LO = CURRENT_STATE.REGS[rs];
				printf("\nMTLO Instruction");
				break;
			case 0x18:
				// done

				product = (product & 0x0000000000000000);
				product = (uint64_t)CURRENT_STATE.REGS[rs] * (uint64_t)CURRENT_STATE.REGS[rt];
				NEXT_STATE.LO = (product & 0X00000000FFFFFFFF);
				NEXT_STATE.HI = (product & 0XFFFFFFFF00000000)>>32;
				printf("\nMULT Instruction");
				break;
			case 0x19:
				// done
				product = (product & 0x0000000000000000);
				if ((CURRENT_STATE.REGS[rs] & 0x80000000) == 0x80000000)
				{
					CURRENT_STATE.REGS[rs] = (~CURRENT_STATE.REGS[rs])+0x00000001;

				}
				if ((CURRENT_STATE.REGS[rt] & 0x80000000) == 0x80000000)
				{
					CURRENT_STATE.REGS[rt] = (~CURRENT_STATE.REGS[rt])+0x00000001;
				}
				product = (uint64_t)CURRENT_STATE.REGS[rs] * (uint64_t)CURRENT_STATE.REGS[rt];
				NEXT_STATE.LO = (product & 0X00000000FFFFFFFF);
				NEXT_STATE.HI = (product & 0XFFFFFFFF00000000)>>32;
				//printf("%"PRIu64"\n",product);
				printf("\nMULTU Instruction");
				break;
			case 0x1A:
				// done
				if(CURRENT_STATE.REGS[rt] != 0)
				{
					NEXT_STATE.LO = CURRENT_STATE.REGS[rs] / CURRENT_STATE.REGS[rt];
					NEXT_STATE.HI = CURRENT_STATE.REGS[rs] % CURRENT_STATE.REGS[rt];
				}
				printf("\nDIV Instruction");
				break;
			case 0x1B:
				// done
				if(CURRENT_STATE.REGS[rt] != 0)
				{
					if ((CURRENT_STATE.REGS[rs] & 0x80000000) == 0x80000000)
					{
						CURRENT_STATE.REGS[rs] = (~CURRENT_STATE.REGS[rs])+0x00000001;

					}
					if ((CURRENT_STATE.REGS[rt] & 0x80000000) == 0x80000000)
					{
						CURRENT_STATE.REGS[rt] = (~CURRENT_STATE.REGS[rt])+0x00000001;
					}

					NEXT_STATE.LO = CURRENT_STATE.REGS[rs] / CURRENT_STATE.REGS[rt];
					NEXT_STATE.HI = CURRENT_STATE.REGS[rs] % CURRENT_STATE.REGS[rt];
				}
				printf("\nDIVU Instruction");
				break;
			case 0x20:
				// done
				NEXT_STATE.REGS[rd] = CURRENT_STATE.REGS[rs] + CURRENT_STATE.REGS[rt];
				printf("\nADD Instruction");
				break;
			case 0x21:
				// put more things here
				printf("\nADDU Instruction");
				if ((CURRENT_STATE.REGS[rs] & 0x80000000) == 0x80000000)
				{
					CURRENT_STATE.REGS[rs] = (~CURRENT_STATE.REGS[rs])+0x00000001;

				}
				if ((CURRENT_STATE.REGS[rt] & 0x80000000) == 0x80000000)
				{
					CURRENT_STATE.REGS[rt] = (~CURRENT_STATE.REGS[rt])+0x00000001;
				}
				NEXT_STATE.REGS[rd] = CURRENT_STATE.REGS[rt] + CURRENT_STATE.REGS[rs];

				break;
			case 0x22:
				// done
				NEXT_STATE.REGS[rd] = CURRENT_STATE.REGS[rs] - CURRENT_STATE.REGS[rt];
				printf("\nSUB Instruction");
				break;
			case 0x23:
				// check
				if ((CURRENT_STATE.REGS[rs] & 0x80000000) == 0x80000000)
				{
					CURRENT_STATE.REGS[rs] = (~CURRENT_STATE.REGS[rs])+0x00000001;

				}
				if ((CURRENT_STATE.REGS[rt] & 0x80000000) == 0x80000000)
				{
					CURRENT_STATE.REGS[rt] = (~CURRENT_STATE.REGS[rt])+0x00000001;
				}
				NEXT_STATE.REGS[rd] = CURRENT_STATE.REGS[rs] - CURRENT_STATE.REGS[rt];
				printf("\nSUBU Instruction");
				break;
			case 0x24:
				// done
				NEXT_STATE.REGS[rd] = CURRENT_STATE.REGS[rs] & CURRENT_STATE.REGS[rt];
				printf("\nAND Instruction");
				break;
			case 0x25:
				// done
				NEXT_STATE.REGS[rd] = CURRENT_STATE.REGS[rs] | CURRENT_STATE.REGS[rt];
				printf("\nOR Instruction");
				break;
			case 0x26:
				// done
				NEXT_STATE.REGS[rd] = CURRENT_STATE.REGS[rs] ^ CURRENT_STATE.REGS[rt];
				printf("\nXOR Instruction");
				break;
			case 0x27:
				// done
				NEXT_STATE.REGS[rd] = ~(CURRENT_STATE.REGS[rs] | CURRENT_STATE.REGS[rt]);
				printf("\nNOR Instruction");
				break;
			case 0x2A:
				// done
				if(CURRENT_STATE.REGS[rs] < CURRENT_STATE.REGS[rt]){
					NEXT_STATE.REGS[rd] = 0x00000001;
				}
				else{
					NEXT_STATE.REGS[rd] = 0x00000000;
				}
				printf("\nSLT Instruction");
				break;
			default:
				// put more things here
				printf("\nCannot Find Instruction");
				break;

			}
		}
		else{
			/*I, J instructions here*/
			switch(opcode){
			case 0x01:
				if(rt == 0x00000){
					// done
					if((CURRENT_STATE.REGS[rs] & 0x80000000) == 0x80000000)
					{							
						if((offset_immediate & 0x00008000) == 0x00008000)
						{
							offset_immediate = ~offset_immediate & 0x0000FFFF;
							offset_immediate = offset_immediate + 0x00000001;
							INSTRUCTION_COUNT = (INSTRUCTION_COUNT-offset_immediate/4)-1;

						}
						else
						{

							INSTRUCTION_COUNT = (INSTRUCTION_COUNT+offset_immediate/4)-1;
						}
					}
					printf("\nBLTZ Instruction");
				}
				else if(rt == 0x00001){
					// check
					if((CURRENT_STATE.REGS[rs] & 0x80000000) == 0x00000000){
						NEXT_STATE.PC = CURRENT_STATE.PC + ((offset_immediate << 18) | (offset_immediate << 2));
					}
					printf("\nBGEZ Instruction");
				}
				break;
			case 0x02:
				// check
				//NEXT_STATE.PC = (CURRENT_STATE.PC & 0xF0000000) | (target << 2);
				if((target & 0x02000000) == 0x002000000)
				{
					target = ~target & 0x03FFFFFF;
					target = target + 0x00000001;
					INSTRUCTION_COUNT = (INSTRUCTION_COUNT-target/4)-1;
				}
				else
				{
					INSTRUCTION_COUNT = INSTRUCTION_COUNT+target/4-1;
				}
				printf("\nJ Instruction");
				break;
			case 0x03:
				// check
				//NEXT_STATE.PC = ((CURRENT_STATE.PC + 0x08) & 0xF0000000) | (target << 2);
				printf("\nJAL Instruction");
				break;
			case 0x04:
				// check
				if(CURRENT_STATE.REGS[rs] == CURRENT_STATE.REGS[rt]){
					if((offset_immediate & 0x00008000) == 0x00008000)
					{
						offset_immediate = ~offset_immediate & 0x0000FFFF;
						offset_immediate = offset_immediate + 0x00000001;
						INSTRUCTION_COUNT = (INSTRUCTION_COUNT-offset_immediate/4)-1;
					}
					else
					{
						INSTRUCTION_COUNT = (INSTRUCTION_COUNT+offset_immediate/4)-1;
					}

				}
				printf("\nBEQ Instruction");
				break;
			case 0x05:
				// check
				if(CURRENT_STATE.REGS[rs] != CURRENT_STATE.REGS[rt]){
					//NEXT_STATE.PC = CURRENT_STATE.PC + ((offset_immediate << 18) | (offset_immediate << 2));
					if((offset_immediate & 0x00008000) == 0x00008000)
					{
						offset_immediate = ~offset_immediate & 0x0000FFFF;
						offset_immediate = offset_immediate + 0x00000001;
						INSTRUCTION_COUNT = (INSTRUCTION_COUNT-offset_immediate/4);
					}
					else
					{
						INSTRUCTION_COUNT = INSTRUCTION_COUNT+offset_immediate/4;
					}


				}

				printf("\nBNE Instruction");
				break;
			case 0x06:
				// check
				if((CURRENT_STATE.REGS[rs] & 0x8000000) == 0x80000000 || CURRENT_STATE.REGS[rs] == 0){
					NEXT_STATE.PC = CURRENT_STATE.PC + ((offset_immediate << 18) | (offset_immediate << 2));
				}
				printf("\nBLEZ Instruction");
				break;
			case 0x07:
				// check
				if((CURRENT_STATE.REGS[rs] & 0x8000000) == 0x80000000 || CURRENT_STATE.REGS[rs] != 0){
					//NEXT_STATE.PC = CURRENT_STATE.PC + ((offset_immediate << 18) | (offset_immediate << 2));
					if((offset_immediate & 0x00008000) == 0x00008000)
					{
						offset_immediate = ~offset_immediate & 0x0000FFFF;
						offset_immediate = offset_immediate + 0x00000001;
						INSTRUCTION_COUNT = (INSTRUCTION_COUNT-offset_immediate/4)-1;
					}
					else
					{
						INSTRUCTION_COUNT = (INSTRUCTION_COUNT+offset_immediate/4)-1;
					}
				}
				printf("\nBGTZ Instruction");
				break;
			case 0x08:
				// check
				NEXT_STATE.REGS[rt] = CURRENT_STATE.REGS[rs] + offset_immediate;
				printf("\n%d,%d\n",CURRENT_STATE.REGS[rs],NEXT_STATE.REGS[rt]);
				printf("\nADDI Instruction");
				break;
			case 0x09:
				// check
			if(((CURRENT_STATE.REGS[rs] & 0x80000000) == 0x80000000) && ((offset_immediate & 0x00008000) == 0x00008000))
				{
					CURRENT_STATE.REGS[rs] = (~CURRENT_STATE.REGS[rs])+0x00000001;
					offset_immediate = (~offset_immediate & 0x0000FFFF)+0x00000001;
					NEXT_STATE.REGS[rt] = CURRENT_STATE.REGS[rs] + offset_immediate;

				}				
				else if ((CURRENT_STATE.REGS[rs] & 0x80000000) == 0x80000000)
				{
					CURRENT_STATE.REGS[rs] = (~CURRENT_STATE.REGS[rs])+0x00000001;
					NEXT_STATE.REGS[rt] = offset_immediate - CURRENT_STATE.REGS[rs];

				}
				else if ((offset_immediate & 0x00008000) == 0x00008000)
				{
					offset_immediate = (~offset_immediate & 0x0000FFFF)+0x00000001;
					NEXT_STATE.REGS[rt] = CURRENT_STATE.REGS[rs] - offset_immediate;
printf("\n%08x\n%08x\n%08x", CURRENT_STATE.REGS[rs],NEXT_STATE.REGS[rt],offset_immediate);
				}
				else
				{
					NEXT_STATE.REGS[rt] = CURRENT_STATE.REGS[rs] + offset_immediate;
				}					
				printf("\nADDIU Instruction");
				break;
case 0x0A:
	// check
	NEXT_STATE.REGS[rt] = CURRENT_STATE.REGS[rs] - offset_immediate;
	NEXT_STATE.REGS[rt] = (NEXT_STATE.REGS[rt]& 0x80000000)>>31;
	printf("\nSLTI Instruction");
	break;
case 0x0C:
	// check
	NEXT_STATE.REGS[rt] = CURRENT_STATE.REGS[rs] & offset_immediate;
	printf("\nANDI Instruction");
	break;
case 0x0D:
	// check
	NEXT_STATE.REGS[rt] = CURRENT_STATE.REGS[rs] | offset_immediate;
	printf("\nORI Instruction");
	break;
case 0x0E:
	// check
	NEXT_STATE.REGS[rt] = (CURRENT_STATE.REGS[rs] ^ offset_immediate);
	printf("\nXORI Instruction");
	break;
case 0x0F:
	// done
	NEXT_STATE.REGS[rt] = offset_immediate << 16;
	printf("\nLUI Instruction");
	break;
case 0x20:
	// check
	NEXT_STATE.REGS[rt] = mem_read_32(offset_immediate + CURRENT_STATE.REGS[rs]);
	printf("\nLB Instruction");
	break;
case 0x21:
	// check
	NEXT_STATE.REGS[rt] = mem_read_32(((~offset_immediate)+1) + CURRENT_STATE.REGS[rs]);
	printf("\nLH Instruction");
	break;
case 0x23:
	// check
	NEXT_STATE.REGS[rt] = mem_read_32(((offset_immediate << 16) | offset_immediate) + CURRENT_STATE.REGS[rs]);
	//printf("\n%08x\n%p\n",NEXT_STATE.REGS[rt],CURRENT_STATE.REGS);
	printf("\nLW Instruction");
	break;
case 0x29:
	// check
	mem_write_32(((~offset_immediate)+1) + CURRENT_STATE.REGS[rs] , CURRENT_STATE.REGS[rt]);
	printf("\nSH Instruction");
	break;
case 0x2B:
	// check
	mem_write_32(((offset_immediate << 16) | offset_immediate) + CURRENT_STATE.REGS[rs] , CURRENT_STATE.REGS[rt]);
	printf("\nSW Instruction");
	//printf("\n%08x\n",((offset_immediate) + CURRENT_STATE.REGS[rs]));
	break;
default:
	// put more things here
	printf("\nCannot Find Instruction");
	break;
			}
		}
		CURRENT_STATE.PC+=4;
		NEXT_STATE.PC+=4;
	}
}


/************************************************************/
/* Initialize Memory                                                                                                    */
/************************************************************/
void initialize() {
	init_memory();
	CURRENT_STATE.PC = MEM_TEXT_BEGIN;
	NEXT_STATE = CURRENT_STATE;
	RUN_FLAG = TRUE;
}

/************************************************************/
/* Print the program loaded into memory (in MIPS assembly format)    */
/************************************************************/
void print_program(){
	/*IMPLEMENT THIS*/
	int i=0;
	uint32_t instruction, opcode, function, rs, rt, rd, sa, offset_immediate, target;

	for(i=0; i<MIPS_REGS; i++){

		instruction = mem_read_32(MEM_REGIONS[0].begin + (0x04 * i));

		opcode = (instruction & 0xFC000000) >> 26;
		function = instruction & 0x0000003F;
		rs = (instruction & 0x03E00000) >> 21;
		rt = (instruction & 0x001F0000) >> 16;
		rd = (instruction & 0x0000F800) >> 11;
		sa = (instruction & 0x000007C0) >> 6;
		offset_immediate = instruction & 0x0000FFFF;
		target = instruction & 0x03FFFFFF;

		if(opcode == 0x00){
			/*R format instructions here*/

			switch(function){
			case 0x00:
				if (sa != 0){
					printf("\nSLL %s, %s, %08x", RegisterName(rd), RegisterName(rt), sa);
				}
				else{
				}
				break;
			case 0x02:
				printf("\nSRL %s, %s, %08x", RegisterName(rd), RegisterName(rt), sa);
				break;
			case 0x03:

				printf("\nSRA %s, %s, %08x", RegisterName(rd), RegisterName(rt), sa);
				break;
			case 0x08:
				printf("\nJR %s", RegisterName(rs));
				break;
			case 0x09:
				if(rd == 0){
					printf("\nJALR %s", RegisterName(rs));
				}
				else{
					printf("\nJALR %s, %s", RegisterName(rd), RegisterName(rs));
				}
				break;
			case 0x0C:
				printf("\nSYSCALL");
				break;
			case 0x10:
				printf("\nMFHI %s", RegisterName(rd));
				break;
			case 0x11:
				printf("\nMTHI %s", RegisterName(rs));
				break;
			case 0x12:
				printf("\nMFLO %s", RegisterName(rd));
				break;
			case 0x13:
				printf("\nMTLO %s", RegisterName(rs));
				break;
			case 0x18:
				printf("\nMULT %s, %s", RegisterName(rs), RegisterName(rt));
				break;
			case 0x19:
				printf("\nMULTU %s, %s", RegisterName(rs), RegisterName(rt));
				break;
			case 0x1A:
				printf("\nDIV %s, %s", RegisterName(rs), RegisterName(rt));
				break;
			case 0x1B:
				printf("\nDIVU %s, %s", RegisterName(rs), RegisterName(rt));
				break;
			case 0x20:
				printf("\nADD %s, %s, %s", RegisterName(rd), RegisterName(rs), RegisterName(rt));
				break;
			case 0x21:
				printf("\nADDU %s, %s, %s", RegisterName(rd), RegisterName(rs), RegisterName(rt));
				break;
			case 0x22:
				printf("\nSUB %s, %s, %s", RegisterName(rd), RegisterName(rs), RegisterName(rt));
				break;
			case 0x23:
				printf("\nSUBU %s, %s, %s", RegisterName(rd), RegisterName(rs), RegisterName(rt));
				break;
			case 0x24:
				printf("\nAND %s, %s, %s", RegisterName(rd), RegisterName(rs), RegisterName(rt));
				break;
			case 0x25:
				printf("\nOR %s, %s, %s", RegisterName(rd), RegisterName(rs), RegisterName(rt));
				break;
			case 0x26:
				printf("\nXOR %s, %s, %s", RegisterName(rd), RegisterName(rs), RegisterName(rt));
				break;
			case 0x27:
				printf("\nNOR %s, %s, %s", RegisterName(rd), RegisterName(rs), RegisterName(rt));
				break;
			case 0x2A:
				printf("\nSLT %s, %s, %s", RegisterName(rd), RegisterName(rs), RegisterName(rt));
				break;
			default:
				printf("\nCannot Find Instruction");
				break;

			}
		}
		else{
			/*I, J instructions here*/
			switch(opcode){
			case 0x01:
				if(rt == 0x00000){
					printf("\nBLTZ %s, %08x", RegisterName(rs), offset_immediate);
				}
				else if(rt == 0x00001){
					printf("\nBGEZ %s, %08x", RegisterName(rs), offset_immediate);
				}
				break;
			case 0x02:
				printf("\nJ %08x", target);
				break;
			case 0x03:
				printf("\nJAL %08x", target);
				break;
			case 0x04:
				printf("\nBEQ %s, %s, %08x", RegisterName(rs), RegisterName(rt), offset_immediate);
				break;
			case 0x05:
				printf("\nBNE %s, %s, %08x", RegisterName(rs), RegisterName(rt), offset_immediate);
				break;
			case 0x06:
				printf("\nBLEZ %s, %08x", RegisterName(rs), offset_immediate);
				break;
			case 0x07:
				printf("\nBGTZ %s, %08x", RegisterName(rs), offset_immediate);
				break;
			case 0x08:
				printf("\nADDI %s, %s, %08x", RegisterName(rt), RegisterName(rs), offset_immediate);
				break;
			case 0x09:
				printf("\nADDIU %s, %s, %08x", RegisterName(rt), RegisterName(rs), offset_immediate);
				break;
			case 0x0A:
				printf("\nSLTI %s, %s, %08x", RegisterName(rt), RegisterName(rs), offset_immediate);
				break;
			case 0x0C:
				printf("\nANDI %s, %s, %08x", RegisterName(rt), RegisterName(rs), offset_immediate);
				break;
			case 0x0D:
				printf("\nORI %s, %s, %08x", RegisterName(rt), RegisterName(rs), offset_immediate);
				break;
			case 0x0E:
				printf("\nXORI %s, %s, %08x", RegisterName(rt), RegisterName(rs), offset_immediate);
				break;
			case 0x0F:
				printf("\nLUI %s, %08x", RegisterName(rt), offset_immediate);
				break;
			case 0x20:
				printf("\nLB %s, %08x(%08x)", RegisterName(rt), offset_immediate, rs);
				break;
			case 0x21:
				printf("\nLH %s, %08x(%08x)", RegisterName(rt), offset_immediate, rs);
				break;
			case 0x23:
				printf("\nLW %s, %08x(%08x)", RegisterName(rt), offset_immediate, rs);
				break;
			case 0x29:
				printf("\nSH %s, %08x(%08x)", RegisterName(rt), offset_immediate, rs);
				break;
			case 0x2B:
				printf("\nSW %s, %08x(%08x)", RegisterName(rt), offset_immediate, rs);
				break;
			default:
				printf("\nCannot Find Instruction");
				break;
			}
		}
	}

}


char* RegisterName(uint32_t number) {

	//char registername[7] = "";
	char *registername = (char*) malloc(7 * sizeof(char));

	switch(number){
	case 0:
		strcpy(registername, "$zero");
		break;
	case 1:
		strcpy(registername, "$at");
		break;
	case 2:
		strcpy(registername, "$v0");
		break;
	case 3:
		strcpy(registername, "$v1");
		break;
	case 4:
		strcpy(registername, "$a0");
		break;
	case 5:
		strcpy(registername, "$a1");
		break;
	case 6:
		strcpy(registername, "$a2");
		break;
	case 7:
		strcpy(registername, "$a3");
		break;
	case 8:
		strcpy(registername, "$t0");
		break;
	case 9:
		strcpy(registername, "$t1");
		break;
	case 10:
		strcpy(registername, "$t2");
		break;
	case 11:
		strcpy(registername, "$t3");
		break;
	case 12:
		strcpy(registername, "$t4");
		break;
	case 13:
		strcpy(registername, "$t5");
		break;
	case 14:
		strcpy(registername, "$t6");
		break;
	case 15:
		strcpy(registername, "$t7");
		break;
	case 16:
		strcpy(registername, "$s0");
		break;
	case 17:
		strcpy(registername, "$s1");
		break;
	case 18:
		strcpy(registername, "$s2");
		break;
	case 19:
		strcpy(registername, "$s3");
		break;
	case 20:
		strcpy(registername, "$s4");
		break;
	case 21:
		strcpy(registername, "$s5");
		break;
	case 22:
		strcpy(registername, "$s6");
		break;
	case 23:
		strcpy(registername, "$s7");
		break;
	case 24:
		strcpy(registername, "$t8");
		break;
	case 25:
		strcpy(registername, "$t9");
		break;
	case 26:
		strcpy(registername, "$k0");
		break;
	case 27:
		strcpy(registername, "$k1");
		break;
	case 28:
		strcpy(registername, "$gp");
		break;
	case 29:
		strcpy(registername, "$sp");
		break;
	case 30:
		strcpy(registername, "$fp");
		break;
	case 31:
		strcpy(registername, "$ra");
		break;
	default:
		strcpy(registername, "ERROR");
		break;

	}
	return registername;

}


/***************************************************************/
/* main                                                                                                                                   */
/***************************************************************/
int main(int argc, char *argv[]) {
	printf("\n**************************\n");
	printf("Welcome to MU-MIPS SIM...\n");
	printf("**************************\n\n");

	if (argc < 2) {
		printf("Error: You should provide input file.\nUsage: %s <input program> \n\n",  argv[0]);
		exit(1);
	}

	strcpy(prog_file, argv[1]);
	initialize();
	load_program();
	help();
	while (1){
		handle_command();
	}
	return 0;
}

